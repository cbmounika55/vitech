// src/reducers/userReducer.js
import { SET_USER } from '../actions/useActions';

// Retrieve name , email from local storage or set it to an empty string if not found
const storedEmail = localStorage.getItem('email') || '';
const storedName = localStorage.getItem('name') || '';
const initialState = {
  name: storedName,
  email: storedEmail, // Initialize email with stored value
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER:
      // Update local storage with the new email and name value
      localStorage.setItem('email', action.payload.email);
      localStorage.setItem('name', action.payload.name);
      return {
        ...state,
        name: action.payload.name,
        email: action.payload.email,
      };
    default:
      return state;
  }
};

export default userReducer;
