import React, { useState } from "react";
import {
  TextField,
  InputAdornment,
  IconButton,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
} from "@mui/material";
import { Search, FilterList, CalendarToday } from "@mui/icons-material";
import Card from "@mui/material/Card";
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";
import SoftPagination from "components/SoftPagination";
import Icon from "@mui/material/Icon";
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
// import Footer from "examples/Footer";
import Table from "examples/Tables/Table";
import authorsTableData from "layouts/tables/data/authorsTableData";
import projectsTableData from "layouts/tables/data/projectsTableData";
import Autocomplete from '@mui/material/Autocomplete';
import { Link } from "react-router-dom";
import SoftButton from "components/SoftButton";
function Tables() {
  const { columns, rows } = authorsTableData;
  // const { columns: prCols, rows: prRows } = projectsTableData;
  const [search, setSearch] = useState("");
  const [anchorEl, setAnchorEl] = useState(null);

  // const handleClick = (event) => {
  //   setAnchorEl(event.currentTarget);
  // };
  const [open, setOpen] = useState(false);
  const [field1, setField1] = useState('');
  const [field2, setField2] = useState('');
  const [field3, setField3] = useState('');

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  
  const handleApply = () => {
    // Handle the apply logic here
    console.log('Field 1:', field1);
    console.log('Field 2:', field2);
    console.log('Field 3:', field3);
    setOpen(false);
  };

  return (
    <DashboardLayout>
      <DashboardNavbar />
      <SoftBox py={3}>
        <SoftBox mb={3}>
          <Card>
            <SoftBox display="flex" justifyContent="space-between" alignItems="center" p={3}>
              <SoftTypography variant="h6">Interview Schedule</SoftTypography>

              <SoftBox display="flex" alignItems="center">
                <TextField
                  variant="outlined"
                  size="small"
                  placeholder="Search"
                  value={search}
                  onChange={(e) => setSearch(e.target.value)}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <IconButton>
                          <Search />
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                  sx={{
                    ".css-15v2jta-MuiInputBase-root-MuiOutlinedInput-root": {
                      height: "46px !important",
                      
                    },
                    marginRight: 1,
                  }}
                />
             
   <div>
   <SoftButton variant="outlined" color="info" size="small" sx={{marginRight: 1 , height:"46px"}} startIcon={<FilterList />} onClick={handleClickOpen}>
                Filter
              </SoftButton>
     {/* <Dialog open={open} onClose={handleClose} maxWidth="md" >
  <DialogTitle>Filter</DialogTitle>
  <DialogContent sx={{ width: 500 }}>
  
    <div style={{ display: 'flex', flexDirection: 'column', gap: '20px' }}>
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <SoftTypography style={{ marginRight: '20px', minWidth: '120px' }}>Job ID</SoftTypography>
        <div style={{ flexGrow: 1 }}>
          <Autocomplete
            disablePortal
            id="name-autocomplete"
            options={top100Films}
            sx={{ width: '100%' }}
            renderInput={(params) => <TextField {...params} placeholder="Job ID"/>}
          />
        </div>
      </div>
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <SoftTypography style={{ marginRight: '20px', minWidth: '120px' }}>Job Title</SoftTypography>
        <div style={{ flexGrow: 1 }}>
          <Autocomplete
            disablePortal
            id="age-autocomplete"
            options={top100Films}
            sx={{ width: '100%' }}
            renderInput={(params) => <TextField {...params} placeholder="Job Title"/>}
          />
        </div>
      </div>
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <SoftTypography style={{ marginRight: '20px', minWidth: '120px' }}>Status</SoftTypography>
        <div style={{ flexGrow: 1 }}>
          <Autocomplete
            disablePortal
            id="location-autocomplete"
            options={top100Films}
            sx={{ width: '100%' }}
            renderInput={(params) => <TextField {...params}  placeholder="Status" />}
          />
        </div>
      </div>
    </div>
  </DialogContent>
  <DialogActions>
    <Button onClick={handleClose} color="primary">
      Reset
    </Button>
    <Button onClick={handleApply} color="primary">
      Apply
    </Button>
  </DialogActions>
</Dialog> */}

    </div>
              
                <Link to="/newSchedule">
                <SoftButton variant="gradient" color="info" size="small" startIcon={<CalendarToday />} sx={{height:"46px"}}>
                New Schedule
                </SoftButton>
              </Link>
              </SoftBox>
            </SoftBox>

            <SoftBox
              sx={{
                "& .MuiTableRow-root:not(:last-child)": {
                  "& td": {
                    borderBottom: ({ borders: { borderWidth, borderColor } }) =>
                      `${borderWidth[1]} solid ${borderColor}`,
                  },
                },
              }}
            >
              <Table columns={columns} rows={rows} />
            </SoftBox>
          </Card>
        </SoftBox>
        <SoftPagination>
          <SoftPagination item>
            <Icon>keyboard_arrow_left</Icon>
          </SoftPagination>
          <SoftPagination item active>1</SoftPagination>
          <SoftPagination item>2</SoftPagination>
          <SoftPagination item>3</SoftPagination>
          <SoftPagination item>
            <Icon>keyboard_arrow_right</Icon>
          </SoftPagination>
        </SoftPagination>
      </SoftBox>
      {/* <Footer /> */}
    </DashboardLayout>
  );
}

export default Tables;

