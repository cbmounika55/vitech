import React, { useState, useEffect } from "react";
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";
import Grid from "@mui/material/Grid";
import Divider from "@mui/material/Divider";
import SoftButton from "components/SoftButton";
import SoftSelect from "components/SoftSelect";
import FormField from "layouts/pages/account/components/FormField";
import { Card } from "@mui/material";
import CompanyTableIcon from "./CompanyTableIcon";
import DataTable from "examples/Tables/DataTable";
import CompanyAddressData from "./CompanyAddressData";
import { GetCountries, GetState, GetCity } from "react-country-state-city";
import { useSelector } from 'react-redux';
import {
  Box,
  Typography,
  Dialog,
  DialogContent,
  TextField,
  Icon,
  DialogActions,
  Button,
} from "@mui/material";
import {useAgencyUIController} from "context";
import { setUpdateAgency } from "context";


function BasicInfo() {
  const [controller, dispatch] = useAgencyUIController();
  const user = useSelector((state) => state.user);
  const { name, email } = user;
  const [formData, setFormData] = useState({
    shortDescription: "",
    companyName: "",
    otherNames: "",
    networkID: "",
    website: "",
    pin: "",
    address: "",
    city: "",
    country: "",
    state: "",
    workinformation:[],
  });
  const [countryCode, setCountryCode] = useState(0);
  const [countryid, setCountryid] = useState(0);
  const [stateid, setStateid] = useState(0);
  const [countryName, setCountryName] = useState("select a country");
  const [stateName, setStateName] = useState("select a State");
  const [cityid, setCityid] = useState(0);
  const [cityName, setCityName] = useState("select a City");
  const [countriesList, setCountriesList] = useState([]);
  const [stateList, setStateList] = useState([]);
  const [cityList, setCityList] = useState([]);
  const [linkDate, setLinkDate] = useState("");

  useEffect(() => {
    GetCountries().then((result) => {
      setCountriesList(result);
      console.log(result);
    });
  }, []);

  const handleChange = (name, value) => {
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
    
    setUpdateAgency(dispatch, {[name]: value})
    console.log(name)  
  };

  const handleSelectChange = (name, value) => {
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };
 const handleCountryChange = (selectedOption) => {
    const country = countriesList[selectedOption];
    console.log(country,"country");
    setCountryCode(country.id)
    setCountryid(country.name);
    setCountryName(country.name);
    setFormData((prevData) => ({
      ...prevData,
      country: country.name,
      state: "",
      city: "",
    }));

    GetState(country.id).then((result) => {
      setStateList(result);
      setCityList([]); // Clear city list when country changes
      setStateid(0); // Reset state selection
      setCityid(0); // Reset city selection
    });
  };
  
  const handleStateChange = (selectedOption) => {
    //const state = stateList[selectedOption];
    setStateid(selectedOption);
    const state =stateList.find((state) => state.id === selectedOption).name
    setStateName(state);
    setFormData((prevData) => ({
      ...prevData,
      state: selectedOption,
      city: "",
    }));
    GetCity(countryCode, selectedOption).then((result) => {
      console.log(countryCode, selectedOption,"city");
      setCityList(result);
      setCityid(0); // Reset city selection
    });
  };
  
  const handleCityChange = (selectedOption) => {
    const city = cityList[selectedOption];
    const cityname = cityList[selectedOption].name;
    console.log(city.name,"city12")
    setCityid(city.id);
    setCityName(cityname);
    setFormData((prevData) => ({
      ...prevData,
      city: city.name,
    }));
  };

  const handleSubmit = async () => {
    const uploadformdata = {
      shortDescription: controller.shortDescription,
    companyName: controller.companyName,
    otherNames: controller.otherNames,
    networkID: controller.networkID,
    website: controller.website,
    pin: controller.pin,
    address: controller.address,
    city: controller.city,
    country: controller.country,
    state: controller.state,
    workinformation:controller.workinformation,
    name: name
    }
    
    try {
      const response = await fetch("http://localhost:5000/api/basic_info", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(uploadformdata),
      });

      if (response.ok) {
        const data = await response.json();
        console.log("Data saved successfully:", data);
        fetchSavedData(); // Fetch and log saved data after saving
      } else {
        console.error("Error saving data:", response.statusText);
      }
    } catch (error) {
      console.error("Error saving data:", error);
    }
  };

  const fetchSavedData = async () => {
    try {
      const response = await fetch("http://localhost:5000/api/get_basic_info", {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      });

      if (response.ok) {
        const data = await response.json();
        console.log("Saved data from MongoDB:", data);
      } else {
        console.error("Error fetching data:", response.statusText);
      }
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  useEffect(() => {
    fetchSavedData();
  }, []);

  const [rows, setRows] = useState(CompanyAddressData.rows);
  const [addressName, setAddressName] = useState("");
  const [addressID, setAddressID] = useState("");
  const [vatID, setVatID] = useState("");
  const [taxID, setTaxID] = useState("");
  const [address, setAddress] = useState("");
  const [country, setCountry] = useState("");
  const [profileStatus, setProfileStatus] = useState("");
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };


  const handleAdd = () => {
    const newRow = {
      addressName,
      addressID,
      vatID,
      taxID,
      address,
      country,
      profileStatus,
      action: <CompanyTableIcon onDelete={() => handleDelete(addressID)} />,
    };
    setRows([...rows, newRow]);
    setUpdateAgency(dispatch, {"workinformation": [...rows,newRow]});
    handleClose();
  };

  const handleDelete = (id) => {
    const updatedRows = rows.filter((row) => row.addressID !== id);
    setRows(updatedRows);
  };

  const columns = [
    { Header: "Address Name", accessor: "addressName", width: "17%" },
    { Header: "Address ID", accessor: "addressID", width: "15%" },
    { Header: "Vat ID", accessor: "vatID", width: "15%" },
    { Header: "Tax ID", accessor: "taxID", width: "15%" },
    { Header: "Address", accessor: "address", width: "15%" },
    { Header: "Country", accessor: "country", width: "15%" },
    { Header: "Profile Status", accessor: "profileStatus", width: "15%" },
    { Header: "Action", accessor: "action" },
  ];

  return (
    <SoftBox mt={5}>
      <Card sx={{ padding: "35px" }}>
        <SoftTypography
          variant="h6"
          mb={2}
          sx={{ marginLeft: "3px", marginTop: "30px", marginBottom: "30px" }}
        >
          Business Information
        </SoftTypography>

        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <FormField
              label="Company Name"
              placeholder="Company Name"
              name="companyName"
              value={formData.companyName}
              onChange={ (e) => handleChange(e.target.name, e.target.value)}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField
              label="Other Names, IF Any"
              placeholder="Other Names, IF Any"
              name="otherNames"
              value={formData.otherNames}
              onChange={ (e) => handleChange(e.target.name, e.target.value)}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField
              label="Network ID"
              placeholder="Network ID"
              name="networkID"
              value={formData.networkID}
              onChange={ (e) => handleChange(e.target.name, e.target.value)}
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <FormField
              label="Website"
              placeholder="Website"
              name="website"
              value={formData.website}
              onChange={ (e) => handleChange(e.target.name, e.target.value)}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField
              label="Short Description"
              multiline
              rows={4}
              placeholder="Short Description"
              name="shortDescription"
              value={formData.shortDescription}
              onChange={ (e) => handleChange(e.target.name, e.target.value)}
            />
          </Grid>
        </Grid>
      </Card>
      <Card sx={{ padding: "35px", marginTop: "40px" }}>
        <SoftTypography
          variant="h6"
          mb={2}
          sx={{ marginLeft: "3px", marginTop: "30px", marginBottom: "30px" }}
        >
          Address
        </SoftTypography>
        <Grid container spacing={3}>
        <Grid item xs={12} sm={6}>
            <SoftBox display="flex" flexDirection="column" justifyContent="flex-end" height="100%">
              <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SoftTypography component="label" variant="caption" fontWeight="bold" textTransform="capitalize">
                  Country
                </SoftTypography>
              </SoftBox>
             
              <SoftSelect
                  // placeholder= {countryName}
                  options={countriesList.map((country, index) => ({
                    value: country.id,
                    label: country.name,
                  }))}
                  onChange={(selectedOption) => {
                    handleCountryChange(selectedOption.value);
                    handleChange("country", selectedOption);
                  }}
                  // value={countriesList.find((option) => option.id === parsedData.id)}
                  value={formData.country} // Make sure countryid is set correctly
                />
            </SoftBox>
          </Grid>
          <Grid item xs={12} sm={4}>
            <SoftBox display="flex" flexDirection="column" justifyContent="flex-end" height="100%">
              <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SoftTypography component="label" variant="caption" fontWeight="bold" textTransform="capitalize">
                  State
                </SoftTypography>
              </SoftBox>
              <SoftSelect
                placeholder = "select a state"
                options={stateList.map((state, index) => ({
                  value: state.id,
                  label: state.name,
                }))}
                onChange={(selectedOption) => {
                  handleStateChange(selectedOption.value);
                  handleChange("state",  selectedOption);
                }}
                value={formData.state}
              />
            </SoftBox>
            
          </Grid>
          <Grid item xs={12} sm={4}>
            <SoftBox display="flex" flexDirection="column" justifyContent="flex-end" height="100%">
              <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SoftTypography component="label" variant="caption" fontWeight="bold" textTransform="capitalize">
                  City
                </SoftTypography>
              </SoftBox>
              <SoftSelect
                placeholder="select a city"
                options={cityList.map((city, index) => ({
                  value: index,
                  label: city.name,
                }))}
                onChange={(selectedOption) => {
                  handleCityChange(selectedOption.value);
                  handleChange("city", selectedOption);
                }}
                value={formData.city}
              />
            </SoftBox>
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField
              label="Pin"
              placeholder="Pin"
              name="pin"
              value={formData.pin}
              onChange={ (e) => handleChange(e.target.name, e.target.value)}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField
              label="Address"
              placeholder="Address"
              multiline
              rows={4}
              name="address"
              value={formData.address}
              onChange={ (e) => handleChange(e.target.name, e.target.value)}
            />
          </Grid>
        </Grid>
      </Card>

      <Card sx={{ marginTop: "40px" }}>
        <SoftBox p={3}>
          <SoftTypography variant="h5">Work Information</SoftTypography>
        </SoftBox>
        <DataTable table={{ columns, rows }} />

        <SoftBox ml={{ xs: 0, sm: 5 }} mt={{ xs: 2, sm: 2 }} mb={{ xs: 2, sm: 5 }}>
          <SoftButton variant="gradient" color="info" size="small" onClick={handleClickOpen}>
            <Icon sx={{ fontWeight: "bold" }}>add</Icon>&nbsp; Add New
          </SoftButton>
          <Dialog open={open} onClose={handleClose} maxWidth="md">
            <DialogContent sx={{ width: 500 }}>
              <Box sx={{ display: "flex", flexDirection: "column", gap: 3, p: 2 }}>
                <Typography variant="h5" sx={{ textAlign: "center" }}>
                  Work Information
                </Typography>
                <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                  <Typography sx={{ minWidth: 120, fontSize: "1rem" }}>Address Name</Typography>
                  <TextField
                    fullWidth
                    placeholder="Address Name"
                    value={addressName}
                    onChange={(e) =>  setAddressName(e.target.value)}
                  />
                </Box>
                <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                  <Typography sx={{ minWidth: 120, fontSize: "1rem" }}>Address ID</Typography>
                  <TextField
                    fullWidth
                    placeholder="Address ID"
                    value={addressID}
                    onChange={(e) => setAddressID(e.target.value)}
                  />
                </Box>
                <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                  <Typography sx={{ minWidth: 120, fontSize: "1rem" }}>VAT ID</Typography>
                  <TextField
                    fullWidth
                    placeholder="VAT ID"
                    value={vatID}
                    onChange={(e) => setVatID(e.target.value)}
                  />
                </Box>
                <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                  <Typography sx={{ minWidth: 120, fontSize: "1rem" }}>Tax ID</Typography>
                  <TextField
                    fullWidth
                    placeholder="Tax ID"
                    value={taxID}
                    onChange={(e) => setTaxID(e.target.value)}
                  />
                </Box>
                <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                  <Typography sx={{ minWidth: 120, fontSize: "1rem" }}>Address</Typography>
                  <TextField
                    fullWidth
                    placeholder="Address"
                    value={address}
                    onChange={(e) => setAddress(e.target.value)}
                  />
                </Box>
                <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                  <Typography sx={{ minWidth: 120, fontSize: "1rem" }}>Country</Typography>
                  <TextField
                    fullWidth
                    placeholder="Country"
                    value={country}
                    onChange={(e) => setCountry(e.target.value)}
                  />
                </Box>
                <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                  <Typography sx={{ minWidth: 120, fontSize: "1rem" }}>Profile Status</Typography>
                  <TextField
                    fullWidth
                    placeholder="Profile Status"
                    value={profileStatus}
                    onChange={(e) => setProfileStatus(e.target.value)}
                  />
                </Box>
              </Box>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose}>Cancel</Button>
              <Button onClick={handleAdd}>Add</Button>
            </DialogActions>
          </Dialog>
        </SoftBox>
      </Card>
      <SoftBox my={3}>
        <SoftButton variant="gradient" color="info" onClick={handleSubmit}>
          Save
        </SoftButton>
      </SoftBox>
      <Divider />
    </SoftBox>
  );
}

export default BasicInfo;
