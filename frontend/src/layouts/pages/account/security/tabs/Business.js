import React, { useEffect, useState } from "react";
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";
import { Card, Grid, InputAdornment, Box, Radio, FormControlLabel } from "@mui/material";
import FormField from "../../components/FormField";
import SoftSelect from "components/SoftSelect";
import SoftButton from "components/SoftButton";
import DataTable from "examples/Tables/DataTable";
import {useAgencyUIController} from "context";
import CompanyTableIcon from "./CompanyTableIcon";
import { setUpdateBusinessData } from "context";
import axios from "axios";
import {
  Typography,
  Dialog,
  DialogContent,
  TextField,
  Icon,
  DialogActions,
  Button,
} from "@mui/material";

 
function Business() {
  const [controller, dispatch] = useAgencyUIController();
  const {name} = controller
  const [businessdata, setBusinessData] = useState({
    yearfounded: "",
    numberofemployes: "",
    networkid: "",
    annuvarevenue: "",
    stocksymbol: "",
    supplierlegalform: "",
    globallocationnumber: "",
    texclassification: "",
    taxtype: "",
    taxid: "",
    statetaxid:"",
    regionaltaxid: "",
    gstrequired: "",
    gstvatid: "",
    gstvatdocuments: "",
    taxclearenced: "",
    taxclearencenumber: "",
    texclearencedocument: "",
    bankinformation:[],
    bussinesstype: "",
    gstvatidradio:"no",
    taxclearenceradio:"no",
    dun50number: [0,0,0],

  });
  const handleSubmit = async () => {
    const {businessData} = controller;
    const uploadBusinessData = {
    yearfounded: businessData.yearfounded,
    numberofemployes: businessData.numberofemployes,
    networkid: businessData.networkid,
    annuvarevenue: businessData.annuvarevenue,
    stocksymbol: businessData.stocksymbol,
    supplierlegalform: businessData.supplierlegalform,
    globallocationnumber: businessData.globallocationnumber,
    texclassification: businessData.texclassification,
    taxtype: businessData.taxtype,
    taxid: businessData.taxid,
    statetaxid: businessData.statetaxid,
    regionaltaxid: businessData.regionaltaxid,
    gstrequired: businessData.gstrequired,
    gstvatid: businessData.gstvatid,
    gstvatdocuments: businessData.gstvatdocuments,
    taxclearenced: businessData.taxclearenced,
    taxclearencenumber: businessData.taxclearencenumber,
    texclearencedocument: businessData.texclearencedocument,
    bankinformation: businessData.bankinformation,
    bussinesstype: businessData.bussinesstype,
    gstvatidradio: businessData.gstvatidradio,
    taxclearenceradio: businessData.taxclearenceradio,
    dun50number: businessData.dun50number.join("-"),
    }
    try {
      const response = await fetch("http://localhost:5000/api/business", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(uploadBusinessData),
      });

      if (response.ok) {
        const data = await response.json();
        console.log("Data saved successfully:", data);
        // fetchSavedData(); // Fetch and log saved data after saving
      } else {
        console.error("Error saving data:", response.statusText);
      }
    } catch (error) {
      console.error("Error saving data:", error);
    }
  };

  // const fetchSavedData = async () => {
  //   try {
  //     const response = await fetch("http://localhost:5000/api/get_business", {
  //       method: "GET",
  //       headers: {
  //         "Content-Type": "application/json",
  //       },
  //     });

  //     if (response.ok) {
  //       const data = await response.json();
  //       console.log("Saved data from MongoDB:", data);
  //     } else {
  //       console.error("Error fetching data:", response.statusText);
  //     }
  //   } catch (error) {
  //     console.error("Error fetching data:", error);
  //   }
  // };

  // useEffect(() => {
  //   // fetchSavedData();
  // }, []);
  
  const handleChange = (value , name ,index = -1) => {
    if (index >-1 && name === "dun50number")
      {
      let newvalue = businessdata.dun50number;
      newvalue[index] = value;
      value = newvalue;
      }
    setBusinessData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
    
    setUpdateBusinessData(dispatch, {[name]: value})
    console.log(name)  
  };

  const [selectedValue, setSelectedValue] = useState("a");
  const [file, setFile] = useState(null);
  useEffect(() => {
    console.log("One of the fields was updated");
  }, [file]);
 

  const handleFileChange = (event) => {
    setFile(event.target.files[0]);
  };
  
  const [rows, setRows] = useState([]);
  const [BankId, setBankId] = useState("");
  const [BankInstitutionName, setBankInstitutionName] = useState("");
  const [AccountHolderName, setAccountHolderName] = useState("");
  const [BranchName, setBranchName] = useState("");
  const [BranchCode, setBranchCode] = useState("");

  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleAdd = () => {
    const newRow = {
      bankId: BankId,
      bankInstitutionName: BankInstitutionName,
      accountHolderName: AccountHolderName,
      branchName: BranchName,
      branchCode: BranchCode,
      action: <CompanyTableIcon onDelete={() => handleDelete(BankId)} />,
    };
    setRows([...rows, newRow]);
    setUpdateBusinessData(dispatch, {"bankinformation": [...rows,newRow]});
    handleClose();
  };

  const handleDelete = (id) => {
    const updatedRows = rows.filter((row) => row.bankId !== id);
    setUpdateBusinessData(dispatch, {"bankinformation": [...updatedRows]});
    setRows(updatedRows);
  };

  const columns = [
    { Header: "Bank ID", accessor: "bankId", width: "17%" },
    { Header: "Bank Institution Name", accessor: "bankInstitutionName", width: "15%" },
    { Header: "Account Holder Name", accessor: "accountHolderName", width: "15%" },
    { Header: "Branch Name", accessor: "branchName", width: "15%" },
    { Header: "Branch Code", accessor: "branchCode", width: "15%" },

    { Header: "Action", accessor: "action" },
  ];
  return (
    <SoftBox mt={5}>
      <Card sx={{ padding: "35px" }}>
        <SoftTypography
          variant="h6"
          mb={2}
          sx={{ marginLeft: "3px", marginTop: "30px", marginBottom: "30px" }}
        >
          Business Information
        </SoftTypography>
 
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <FormField label="Year Founded" name = "yearfounded" placeholder="Year Founded" value = {businessdata.yearfounded}
            onChange = {(e) => handleChange(e.target.value , e.target.name)}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField label="Number Of Employees" name = "numberofemployes"  placeholder="Number Of Employees" value = {businessdata.numberofemployes}
            onChange = {(e) => handleChange(e.target.value , e.target.name)}
                        />
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField label="Network ID" name = "networkid" placeholder="Network ID" value = {businessdata.networkid}
                        onChange = {(e) => handleChange(e.target.value , e.target.name)}
                        />
          </Grid>
          <Grid item xs={12} sm={6}>
            <SoftBox display="flex" flexDirection="column" justifyContent="flex-end" height="100%">
              <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SoftTypography 
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  Annual Revenue
                </SoftTypography>
              </SoftBox>
              <SoftSelect
                placeholder="Annual Revenue" name = "annuvarevenue" 
                options={[
                  {
                    value: "Sales-focused revenue analysis",
                    label: "Sales-focused revenue analysis",
                  },
                  {
                    value: "Revenue analysis by department",
                    label: "Revenue analysis by department",
                  },
                  { value: "Revenue analysis by product", label: "Revenue analysis by product" },
                ]} 
                value = {businessdata.annuvarevenue}
                onChange = {(selectedOption) => handleChange(selectedOption , "annuvarevenue")}
                />
            </SoftBox>
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField label="Stock Symbol" name = "stocksymbol" placeholder="Stock Symbol" value = {businessdata.stocksymbol}
                        onChange = {(e) => handleChange(e.target.value , e.target.name)}
                        />
          </Grid>
        </Grid>
      </Card>
 
      <Card sx={{ padding: "35px", marginTop: "40px" }}>
        <SoftTypography
          variant="h6"
          mb={2}
          sx={{ marginLeft: "3px", marginTop: "30px", marginBottom: "30px" }}
        >
          Financial Information
        </SoftTypography>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <SoftBox display="flex" flexDirection="column" justifyContent="flex-end" height="100%">
              <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SoftTypography
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  Supplier Legal form
                </SoftTypography>
              </SoftBox>
              <SoftSelect
                placeholder="Supplier Legal form"
                options={[
                  { value: "India", label: "India" },
                  { value: "Asia", label: "Austria" },
                  { value: "USA", label: "USA" },
                ]}
                name = "supplierlegalform" value = {businessdata.supplierlegalform}
                onChange = {(selectedOption) => handleChange(selectedOption , "supplierlegalform")}
                />
            </SoftBox>
          </Grid>
          <Grid item xs={12} sm={6}>
            <SoftBox display="flex" flexDirection="column" justifyContent="flex-end" height="100%">
              <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SoftTypography
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  D-U-N_50 Number
                </SoftTypography>
              </SoftBox>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={4}>
                  <FormField
                    placeholder="0"
                    value= {businessdata.dun50number[0]}
                    name="dun50number"
                    inputType="number"
                    InputProps={{
                      endAdornment: <InputAdornment position="end">d</InputAdornment>,
                      inputProps: { min: 0 },
                      
                    }}
                    onChange = {(e) => handleChange(e.target.value , "dun50number",0)}

                  />
                </Grid>
                <Grid item xs={12} sm={4}>
                  <FormField
                    placeholder="0"
                    name="dun50number"
                    value= {businessdata.dun50number[1]}
                    inputType="number"
                    InputProps={{
                      endAdornment: <InputAdornment position="end">h</InputAdornment>,
                      inputProps: { min: 0, max: 23 },
                    }}
                    onChange = {(e) => handleChange(e.target.value , "dun50number",1)}

                  />
                </Grid>
                <Grid item xs={12} sm={4}>
                  <FormField
                    placeholder="0"
                    name="dun50number"
                    value= {businessdata.dun50number[2]}
                    inputType="number"
                    InputProps={{
                      endAdornment: <InputAdornment position="end">m</InputAdornment>,
                      inputProps: { min: 0, max: 59 },
                    }}
                    onChange = {(e) => handleChange(e.target.value , "dun50number",2)}

                  />
                </Grid>
              </Grid>
            </SoftBox>
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField label="Global Location Number" name = "globallocationnumber" placeholder="Global Location Number" value = {businessdata.globallocationnumber}
                        onChange = {(e) => handleChange(e.target.value , e.target.name)}
                        />
          </Grid>
        </Grid>
      </Card>
 
      <Card sx={{ padding: "35px", marginTop: "40px" }}>
        <SoftTypography
          variant="h6"
          mb={2}
          sx={{ marginLeft: "3px", marginTop: "30px", marginBottom: "30px" }}
        >
          Tax Information
        </SoftTypography>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <SoftBox display="flex" flexDirection="column" justifyContent="flex-end" height="100%">
              <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SoftTypography
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  Tax Classification
                </SoftTypography>
              </SoftBox>
              <SoftSelect
                placeholder="Tax Classification" name = "texclassification" 
                options={[
                  { value: "India", label: "India" },
                  { value: "Asia", label: "Austria" },
                  { value: "USA", label: "USA" },
                ]}
             value = {businessdata.texclassification}
             onChange = {(selectedOption) => handleChange(selectedOption , "texclassification")}
             />
              
            </SoftBox>
          </Grid>
          <Grid item xs={12} sm={6}>
            <SoftBox display="flex" flexDirection="column" justifyContent="flex-end" height="100%">
              <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SoftTypography
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  Taxation Type
                </SoftTypography>
              </SoftBox>
              <SoftSelect
                placeholder="Taxation Type" name = "taxtype" 
                options={[
                  { value: "India", label: "India" },
                  { value: "Asia", label: "Austria" },
                  { value: "USA", label: "USA" },
                ]}
                value = {businessdata.taxtype}
                onChange = {(selectedOption) => handleChange(selectedOption , "taxtype")}
                />

            </SoftBox>
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField label="Tax ID" name = "taxid"  placeholder="Tax ID" value = {businessdata.taxid}
                        onChange = {(e) => handleChange(e.target.value , e.target.name)}
                        />
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField label="State Tax ID" name = "statetaxid"  placeholder="State Tax ID" value = {businessdata.statetaxid}
                        onChange = {(e) => handleChange(e.target.value , e.target.name)}
                        />
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField label="Regional Tax ID" name = "regionaltaxid" placeholder="Regional Tax ID" value = {businessdata.regionaltaxid}
                        onChange = {(e) => handleChange(e.target.value , e.target.name)}
                        />
          </Grid>
          <Grid item xs={12} sm={6}>
            <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
              <SoftTypography
                component="label"
                variant="caption"
                fontWeight="bold"
                textTransform="capitalize"
              >
                GST/Vat Registered
              </SoftTypography>
            </SoftBox>
            <Box sx={{ display: "flex", gap: 2, marginLeft: "20px" }}>
              <FormControlLabel
                control={
                  <Radio
                  checked={businessdata.gstvatidradio === "yes"}
                    onChange = {(e) => handleChange(e.target.value , e.target.name)}
                    value="yes"
                    name="gstvatidradio"
                    inputProps={{ "aria-label": "A" }}
                  />
                }
                label="Yes"
              />
              <FormControlLabel
                control={
                  <Radio
                  checked={businessdata.gstvatidradio === "no"}
                    onChange = {(e) => handleChange(e.target.value , e.target.name)}
                    value="no"
                    name="gstvatidradio"
                    inputProps={{ "aria-label": "B" }}
                  />
                }
                label="No"
              />
            </Box>
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField label="GST /Vat ID" name = "gstvatid" placeholder="GST /Vat ID" value = {businessdata.gstvatid}
                        onChange = {(e) => handleChange(e.target.value , e.target.name)}
                        />
          </Grid>
          <Grid item xs={12} sm={6}>
            <SoftBox display="flex" flexDirection="column" justifyContent="flex-end" height="100%">
              <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SoftTypography
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  GST/VAT Registration Document
                </SoftTypography>
              </SoftBox>
              <FormField type="file" onChange = {(e) => handleChange(e.target.files[0] , e.target.name)} name = "gstvatdocuments"         accept=".pdf"
 />
            </SoftBox>
          </Grid>
          <Grid item xs={12} sm={6}>
            <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
              <SoftTypography
                component="label"
                variant="caption"
                fontWeight="bold"
                textTransform="capitalize"
              >
                Tax Clearance
              </SoftTypography>
            </SoftBox>
            <Box sx={{ display: "flex", gap: 2, marginLeft: "20px" }}>
              <FormControlLabel
                control={
                  <Radio
                  checked={businessdata.taxclearenceradio === "yes"}
                  onChange = {(e) => handleChange(e.target.value , e.target.name)}
                  value="yes"
                  name="taxclearenceradio"
                  inputProps={{ "aria-label": "A" }}
                  />
                }
                label="Yes"
              />
              <FormControlLabel
                control={
                  <Radio
                  checked={businessdata.taxclearenceradio === "no"}
                  onChange = {(e) => handleChange(e.target.value , e.target.name)}
                  value="no"
                  name="taxclearenceradio"
                  inputProps={{ "aria-label": "A" }}
                  />
                }
                label="No"
              />
            </Box>
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField label="Tax Clearance No" name = "taxclearenced" placeholder="Tax Clearance No" value = {businessdata.taxclearenced}
                        onChange = {(e) => handleChange(e.target.value , e.target.name)}
                        />
          </Grid>
          <Grid item xs={12} sm={6}>
            <SoftBox display="flex" flexDirection="column" justifyContent="flex-end" height="100%">
              <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SoftTypography
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  Tax Clearance Document
                </SoftTypography>
              </SoftBox>
              <FormField type="file" onChange = {(e) => handleChange(e.target.files[0] , e.target.name)} name = "texclearencedocument" accept=".pdf"
 />            </SoftBox>
          </Grid>
        </Grid>
      </Card>
 
      <SoftBox my={3}>
        <Card>
          <SoftTypography
            variant="h6"
            mb={2}
            sx={{ marginLeft: "30px", marginTop: "30px", marginBottom: "30px" }}
          >
            Bank Information
          </SoftTypography>
          <DataTable table={{ columns, rows }} />
          <SoftBox ml={{ xs: 0, sm: 5 }} mt={{ xs: 2, sm: 2 }} mb={{ xs: 2, sm: 5 }}>
            <SoftButton variant="gradient" color="info" size="small" onClick={handleClickOpen}>
              <Icon sx={{ fontWeight: "bold" }}>add</Icon>&nbsp; Add New
            </SoftButton>
            <Dialog open={open} onClose={handleClose} maxWidth="md">
              <DialogContent sx={{ width: 500 }}>
                <Box sx={{ display: "flex", flexDirection: "column", gap: 3, p: 2 }}>
                  <Typography variant="h5" sx={{ textAlign: "center" }}>
                    Bank Information
                  </Typography>
                  <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                    <Typography sx={{ minWidth: 120, fontSize: "1rem" }}>Bank ID</Typography>
                    <TextField
                      fullWidth
                      placeholder="Bank ID"
                      value={BankId}
                      onChange={(e) => setBankId(e.target.value)}
                    />
                  </Box>
                  <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                    <Typography sx={{ minWidth: 120, fontSize: "1rem" }}>
                      Bank Institution Name
                    </Typography>
                    <TextField
                      fullWidth
                      placeholder="Bank Institution Name"
                      value={BankInstitutionName}
                      onChange={(e) => setBankInstitutionName(e.target.value)}
                    />
                  </Box>
                  <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                    <Typography sx={{ minWidth: 120, fontSize: "1rem" }}>
                      Account Holder Name
                    </Typography>
                    <TextField
                      fullWidth
                      placeholder="Account Holder Name"
                      value={AccountHolderName}
                      onChange={(e) => setAccountHolderName(e.target.value)}
                    />
                  </Box>
                  <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                    <Typography sx={{ minWidth: 120, fontSize: "1rem" }}>Branch Name</Typography>
                    <TextField
                      fullWidth
                      placeholder="Branch Name"
                      value={BranchName}
                      onChange={(e) => setBranchName(e.target.value)}
                    />
                  </Box>

                  <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                    <Typography sx={{ minWidth: 120, fontSize: "1rem" }}>Branch Code</Typography>
                    <TextField
                      fullWidth
                      placeholder="Branch Code"
                      value={BranchCode}
                      onChange={(e) => setBranchCode(e.target.value)}
                    />
                  </Box>
                </Box>
              </DialogContent>
              <DialogActions>
                <Button onClick={handleClose}>Cancel</Button>
                <Button onClick={handleAdd}>Add</Button>
              </DialogActions>
            </Dialog>
          </SoftBox>
        </Card>
      </SoftBox>
      {/* <BackInformationList /> */}
      <Card sx={{ padding: "35px", marginTop: "40px" }}>
        <SoftTypography
          variant="h6"
          mb={2}
          sx={{ marginLeft: "3px", marginTop: "30px", marginBottom: "30px" }}
        >
          Business Type
        </SoftTypography>
        <Grid item xs={12} sm={6}>
          <SoftBox display="flex" flexDirection="column" justifyContent="flex-end" height="100%">
            <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
              <SoftTypography
                component="label"
                variant="caption"
                fontWeight="bold"
                textTransform="capitalize"
              >
                Business Type
              </SoftTypography>
            </SoftBox>
            <SoftSelect
              placeholder="Annual Revenue" name = "bussinesstype" 
              options={[
                {
                  value: "Sales-focused revenue analysis",
                  label: "Sales-focused revenue analysis",
                },
                {
                  value: "Revenue analysis by department",
                  label: "Revenue analysis by department",
                },
                { value: "Revenue analysis by product", label: "Revenue analysis by product" },
              ]}
              value = {businessdata.bussinesstype}
              onChange = {(selectedOption) => handleChange(selectedOption , "bussinesstype")}
              />

          </SoftBox>
        </Grid>
        <SoftBox my={3}>
          <SoftButton variant="gradient" color="info" sx={{ marginRight: "15px" }} onClick={handleSubmit}>
            Save
          </SoftButton>
       
        </SoftBox>
      </Card>
    </SoftBox>
  );
}
 
export default Business;