import Card from "@mui/material/Card";
import React, { useState } from "react";
import {
  Box,
  Typography,
  Dialog,
  DialogContent,
  TextField,
  Icon,
  DialogActions,
  Button,
} from "@mui/material";
// Assuming Soft UI Dashboard PRO React components and custom components are imported properly
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";
import SoftButton from "components/SoftButton";
import DataTable from "examples/Tables/DataTable";
import ActionCell from "layouts/ecommerce/products/products-list/components/ActionCell";
import { useProfileUIController } from "context";
import { setUpdateProfile } from "context";

// Data
// import dataTableData from "layouts/applications/data-tables/data/dataTableData";

function ChangePassword() {
  const [rows, setRows] = useState([]);
  const [CompanyName, setCompanyName] = useState("");
  const [StartDate, setStartDate] = useState("");
  const [Enddate, setEnddate] = useState("");
  const [open, setOpen] = useState(false);
  const [controller, dispatch] = useProfileUIController();
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleAdd = () => {
    const newRow = {
      companyName: CompanyName,
      startDate: StartDate,
      endDate: Enddate,
      action: <ActionCell />,
    };
    setRows([...rows, newRow]);
    setUpdateProfile(dispatch, {"workinformation": [...rows,newRow]});
    handleClose();
  };
  const columns = [
    { Header: "company name", accessor: "companyName", width: "20%" },
    { Header: "start date", accessor: "startDate" },
    { Header: "end date", accessor: "endDate" },
    { Header: "action", accessor: "action" },
  ];

  return (
    <Card id="work-information">
      <SoftBox p={3}>
        <SoftTypography variant="h5">Work Information</SoftTypography>
      </SoftBox>
      <DataTable table={{ columns, rows }} />
      {/* <DataTable table={dataTableData} /> */}

      <SoftBox ml={{ xs: 0, sm: 5 }} mt={{ xs: 2, sm: 2 }} mb={{ xs: 2, sm: 5 }}>
        <SoftButton variant="gradient" color="info" size="small" onClick={handleClickOpen}>
          <Icon sx={{ fontWeight: "bold" }}>add</Icon>&nbsp; Add New
        </SoftButton>
        <Dialog open={open} onClose={handleClose} maxWidth="md">
          <DialogContent sx={{ width: 500 }}>
           
            <Box sx={{ display: "flex", flexDirection: "column", gap: 3, p: 2 }}>
              <Typography variant="h5" sx={{ textAlign: "center" }}>
                Work Information
              </Typography>
              <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                <Typography sx={{ minWidth: 120, fontSize: "1rem" }}>Company Name</Typography>
                <TextField
                  fullWidth
                  placeholder="Company Name"
                  value={CompanyName}
                  onChange={(e) => setCompanyName(e.target.value)}
                />
              </Box>
              <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                <Typography sx={{ minWidth: 120, fontSize: "1rem" }}>Start Date</Typography>
                <TextField
                  fullWidth
                  type="date"
                  placeholder="Start Date"
                  value={StartDate}
                  onChange={(e) => setStartDate(e.target.value)}
                />
              </Box>
              <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                <Typography sx={{ minWidth: 120, fontSize: "1rem" }}>End Date</Typography>
                <TextField
                  fullWidth
                  type="date"
                  placeholder="End Date"
                  value={Enddate}
                  onChange={(e) => setEnddate(e.target.value)}
                />
              </Box>
            </Box>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>Cancel</Button>
            <Button onClick={handleAdd}>Add</Button>
          </DialogActions>
        </Dialog>
      </SoftBox>
    </Card>
  );
}

export default ChangePassword;
