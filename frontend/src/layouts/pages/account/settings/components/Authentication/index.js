

import Card from "@mui/material/Card";
import React, { useState } from "react";
import { Box, Typography, Dialog, DialogContent, TextField, Icon, DialogActions, Button } from "@mui/material";
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";
import SoftButton from "components/SoftButton";
import DataTable from "examples/Tables/DataTable";
import ActionCell from "layouts/ecommerce/products/products-list/components/ActionCell";
import { useProfileUIController } from "context";
import { setUpdateProfile } from "context";
import dataTableEducation from "layouts/applications/data-tables/data/dataTableEducation";

function Authentication() {
  const [rows, setRows] = useState(dataTableEducation.rows);
  const [Institution, setInstitution] = useState('');
  const [qualification, setQualification] = useState('');
  const [classCgpa, setClass] = useState('');
  const [FromDate, setFromDate] = useState('');
  const [Todate, setTodate] = useState('');
  const [open, setOpen] = useState(false);
  const [controller, dispatch] = useProfileUIController();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleAdd = () => {
    const newRow = {
      institutionName: Institution,
      qualification:qualification,
      classCgpa:classCgpa,
      fromDate: FromDate,
      toDate: Todate,
      action: <ActionCell />,
    };
    setRows([...rows, newRow]);
    setUpdateProfile(dispatch, {"educationhistory": [...rows,newRow]});
    handleClose();
  };

  const columns = [
    { Header: "Institution name", accessor: "institutionName", width: "20%" },
    { Header: " Qualification" , accessor: "qualification" },
    { Header: " Class / CGPA" , accessor: "classCgpa" },
    { Header: "from date", accessor: "fromDate" },
    { Header: "to date", accessor: "toDate" },
    { Header: "action", accessor: "action" },

  ];

  return (
    <Card id="education-history" sx={{ overflow: "visible" }}>
      <SoftBox display="flex" justifyContent="space-between" alignItems="center" p={3}>
        <SoftTypography variant="h5">Education History</SoftTypography>
      </SoftBox>
      <DataTable table={{ columns, rows }} />
      <SoftBox ml={{ xs: 0, sm: 5 }} mt={{ xs: 2, sm: 2 }} mb={{ xs: 2, sm: 5 }}>
        <SoftButton variant="gradient" color="info" size="small" onClick={handleClickOpen}>
          <Icon sx={{ fontWeight: "bold" }}>add</Icon>&nbsp; Add New
        </SoftButton>
        <Dialog open={open} onClose={handleClose} maxWidth="md">
          <DialogContent sx={{ width: 500 }}>
            <Box sx={{ display: 'flex', flexDirection: 'column', gap: 3, p: 3 }}>
              <Typography variant="h5" sx={{ textAlign: 'center' }}>Education History</Typography>
              <Box sx={{ display: 'flex', alignItems: 'center' }}>
                <Typography sx={{ mr: 2.5, minWidth: 120, fontSize: '1rem' }}>Institution Name</Typography>
                <TextField
                  fullWidth
                  placeholder="Institution"
                  value={Institution}
                  onChange={(e) => setInstitution(e.target.value)}
                />
              </Box>
              <Box sx={{ display: 'flex', alignItems: 'center', gap: 2 }}>
        <Typography sx={{ minWidth: 120, fontSize: '1rem' }}>Qualification</Typography>
        <TextField
          fullWidth
          placeholder=" Qualification"
          value={qualification}
          onChange={(e) => setQualification(e.target.value)}
        />
      </Box>
      <Box sx={{ display: 'flex', alignItems: 'center', gap: 2 }}>
        <Typography sx={{ minWidth: 120, fontSize: '1rem' }}>Class / CGPA</Typography>
        <TextField
          fullWidth
          placeholder="Class/CGPA"
          value={qualification}
          onChange={(e) => setClass(e.target.value)}
        />
      </Box>
              <Box sx={{ display: 'flex', alignItems: 'center' }}>
                <Typography sx={{ mr: 2.5, minWidth: 120, fontSize: '1rem' }}>From Date</Typography>
                <TextField
                  fullWidth
                  type="date"
                  placeholder="From Date"
                  value={FromDate}
                  onChange={(e) => setFromDate(e.target.value)}
                />
              </Box>
              <Box sx={{ display: 'flex', alignItems: 'center' }}>
                <Typography sx={{ mr: 2.5, minWidth: 120, fontSize: '1rem' }}>To date</Typography>
                <TextField
                  fullWidth
                  type="date"
                  placeholder="To date"
                  value={Todate}
                  onChange={(e) => setTodate(e.target.value)}
                />
              </Box>
            </Box>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>Cancel</Button>
            <Button onClick={handleAdd}>Add</Button>
          </DialogActions>
        </Dialog>
      </SoftBox>
    </Card>
  );
}

export default Authentication;

