import React, { useState } from 'react';
import ReactApexChart from 'react-apexcharts';
import { Card, CardContent, Grid } from '@mui/material';

function DualChart() {
  const [barChartOptions, setBarChartOptions] = useState({
    series: [
      {
        name: 'Data',
        data: [120, 200, 150, 80, 70, 110, 130],
      },
    ],
    options: {
      chart: {
        type: 'bar',
        height: 350,
      },
      xaxis: {
        categories: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow',
        },
      },
    },
  });

  const funnelChartOptions = {
    series: [
      {
        name: 'Funnel Series',
        data: [30, 40, 50, 70, 100] // Reverse the data array
      },
    ],
    options: {
      chart: {
        type: 'bar',
        height: 350,
        events: {
          dataPointSelection: (event, chartContext, config) => {
            const clickedCategory = funnelChartOptions.options.xaxis.categories[config.dataPointIndex];
            if (clickedCategory === 'Interviewed') {
              const pieChartOptions = {
                series: [30, 20, 10, 40],
                options: {
                  chart: {
                    type: 'pie',
                    height: 350,
                  },
                  labels: ['Category A', 'Category B', 'Category C', 'Category D'],
                  tooltip: {
                    enabled: true,
                    formatter: function (val, opt) {
                      return opt.w.globals.labels[opt.seriesIndex] + ': ' + val;
                    },
                  },
                },
              };
              setBarChartOptions(pieChartOptions);
            } else if (clickedCategory === 'Onboarded') {
              setBarChartOptions({
                series: [
                  {
                    name: 'Data',
                    data: [200, 170, 130, 180, 30, 10, 70],
                  },
                ],
                options: {
                  chart: {
                    type: 'bar',
                    height: 350,
                  },
                  xaxis: {
                    categories: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
                  },
                  tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                      type: 'shadow',
                    },
                  },
                },
              });
            } else {
              setBarChartOptions({
                series: [
                  {
                    name: 'Data',
                    data: [120, 200, 150, 80, 70, 110, 130],
                  },
                ],
                options: {
                  chart: {
                    type: 'bar',
                    height: 350,
                  },
                  xaxis: {
                    categories: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
                  },
                  tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                      type: 'shadow',
                    },
                  },
                },
              });
            }
          },
        },
      },
      plotOptions: {
        bar: {
          borderRadius: 0,
          horizontal: true,
          barHeight: '80%',
          isFunnel: true,
          colors: {
            ranges: [
              { from: 0, to: 20, color: '#ab47bc' },
              { from: 20, to: 40, color: '#ff5722' },
              { from: 40, to: 60, color: '#ffa000' },
              { from: 60, to: 80, color: '#29b6f6' },
              { from: 80, to: 100, color: '#26c6da' },
            ],
          },
        },
      },
      dataLabels: {
        enabled: true,
        formatter: function (val, opt) {
          return opt.w.globals.labels[opt.dataPointIndex] + ': ' + val;
        },
        dropShadow: {
          enabled: true,
        },
      },
      xaxis: {
        categories: [
          'Total Applicants',
          'Interviewed',
          'Shortlisted',
          'Offered',
          'Onboarded',    
        ].reverse(), // Reverse the categories array
      },
      legend: {
        show: false,
      },
    },
  };

  return (
    <Grid container spacing={2} justifyContent="center">
      <Grid item xs={12}>
        <Card sx={{ margin: '0 auto', mt: 4, backgroundColor: 'white' }}>
          <CardContent>
            <Grid container spacing={2}>
              <Grid item xs={12} md={6}>
                <h4>Recruitment Funnel</h4>
                <ReactApexChart
                  options={funnelChartOptions.options}
                  series={funnelChartOptions.series}
                  type="bar"
                  height={350}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <h4>Total Applicants</h4>
                <ReactApexChart
                  options={barChartOptions.options}
                  series={barChartOptions.series}
                  type={barChartOptions.options.chart.type}
                  height={350}
                />
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
}

export default DualChart;
