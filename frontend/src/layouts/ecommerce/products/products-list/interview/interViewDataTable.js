// Import PropTypes
import PropTypes from "prop-types";

// Soft UI Dashboard PRO components
import SoftBadge from "components/SoftBadge";

// ProductsList page components
import ProductCell from "layouts/ecommerce/products/products-list/components/ProductCell";
import ActionCell from "layouts/ecommerce/products/products-list/components/ActionCell";

// Images
import macBookPro from "assets/images/ecommerce/macbook-pro.jpeg";

// Badges
const outOfStock = (
  <SoftBadge variant="contained" color="error" size="xs" badgeContent="inactive" container />
);
const inStock = (
  <SoftBadge variant="contained" color="success" size="xs" badgeContent="active" container />
);
const complete = (
  <SoftBadge variant="contained" color="warning" size="xs" badgeContent="complete" container />
);

const InterViewDataTable = {
  columns: [
    {
      Header: "Start date",
      accessor: "startdate",
      width: "17%",
      // eslint-disable-next-line react/prop-types
      Cell: ({ value }) => {
        const [name, data] = value;
        return <ProductCell image={data.image} name={name} checked={data.checked} />;
      },
    },
    { Header: "End date", accessor: "enddate", width: "15%" },
    { Header: "Created date", accessor: "createdDate", width: "15%" },
    {
      Header: "status",
      accessor: "status",
      // eslint-disable-next-line react/prop-types
      Cell: ({ value }) => {
        if (value === "active") return inStock;
        if (value === "complete") return complete;
        return outOfStock;
      },
    },
    { Header: "action", accessor: "action" },
  ],

  rows: [
    {
      startdate: ["09 may 24", { image: macBookPro, checked: true }],
      enddate: "10 may 24",
      createdDate: "09 may 24",
      status: "active",
      action: <ActionCell />,
    },
    {
      startdate: ["14 may 24", { image: macBookPro, checked: false }],
      enddate: "17 may 24",
      createdDate: "14 may 24",
      status: "inactive",
      action: <ActionCell />,
    },
    {
      startdate: ["16 may 24", { image: macBookPro, checked: false }],
      enddate: "17 may 24",
      createdDate: "16 may 24",
      status: "complete",
      action: <ActionCell />,
    },
  ],
};

// PropTypes validation for Cell components
const CellPropTypes = {
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.array,
  ]).isRequired,
};

// Apply PropTypes validation to Cell components
InterViewDataTable.columns.forEach((column) => {
  if (column.Cell) {
    column.Cell.propTypes = CellPropTypes;
  }
});

export default InterViewDataTable;
