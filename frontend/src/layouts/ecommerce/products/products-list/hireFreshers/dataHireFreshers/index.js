/**
=========================================================
* Soft UI Dashboard PRO- v4.0.2
=========================================================

* Product Page: https://www.creative-tim.com/product/soft-ui-dashboard-pro-react
* Copyright 2023 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

/* eslint-disable react/prop-types */


// ProductsList page components
//import ProductCell from "layouts/ecommerce/products/products-list/components/ProductCell";
import ProductCell from "../../components/ProductCell";
// import ActionCell from "layouts/ecommerce/products/products-list/components/ActionCell";

// Images
// import adidasHoodie from "assets/images/ecommerce/adidas-hoodie.jpeg";
import macBookPro from "assets/images/ecommerce/macbook-pro.jpeg";
// import metroChair from "assets/images/ecommerce/metro-chair.jpeg";
// import alchimiaChair from "assets/images/ecommerce/alchimia-chair.jpeg";
// import fendiCoat from "assets/images/ecommerce/fendi-coat.jpeg";
// import offWhiteJacket from "assets/images/ecommerce/off-white-jacket.jpeg";
// import yohjiYamamoto from "assets/images/ecommerce/yohji-yamamoto.jpeg";
// import mcqueenShirt from "assets/images/ecommerce/mcqueen-shirt.jpeg";
// import yellowChair from "assets/images/ecommerce/yellow-chair.jpeg";
// import heronTshirt from "assets/images/ecommerce/heron-tshirt.jpeg";
// import livingChair from "assets/images/ecommerce/living-chair.jpeg";
// import orangeSofa from "assets/images/ecommerce/orange-sofa.jpeg";
// import burberry from "assets/images/ecommerce/burberry.jpeg";
// import dgSkirt from "assets/images/ecommerce/d&g-skirt.jpeg";
// import undercover from "assets/images/ecommerce/undercover.jpeg";

import DownloadIcon from "../../components/ActionCell/DownloadIcon";
import EventAvailableIcon from "../../components/ActionCell/EventAvailableIcon";

// import EventAvailableIcon from "../components/ActionCell/EventAvailableIcon";

// Badges


const dataHireFreshers = {
  columns: [
    {
      Header: "Candidate Name",
      accessor: "candidateName",
      width: "17%",
      Cell: ({ value: [name, data] }) => (
        <ProductCell image={data.image} name={name} checked={data.checked} />
      ),
    },
    { Header: "Job Name", accessor: "jobName", width: "15%" },
    { Header: "Vendor Name", accessor: "vendorName", width: "15%" },
    { Header: "Email", accessor: "email", width: "15%" },
    { Header: "mobile no", accessor: "mobileNo", width: "15%" },
    { Header: "Resume/video", accessor: "video", width: "15%" },
    { Header: "action", accessor: "action" },
  ],

  rows: [
    {
      candidateName: ["Indrazith M", { image: macBookPro, checked: false }],
      jobName: "Quality Analyst",
      vendorName: "Smart Recruiters",
      email: "indrazith@gmail.com",
      mobileNo: 9876543210,
      video: <DownloadIcon />,
      action: <EventAvailableIcon />,
    },
    {
      candidateName: ["Vignesh S", { image: macBookPro, checked: false }],
      jobName: "Software Developer",
      vendorName: "Talent Hub Connect",
      email: "vignesh@gmail.com",
      mobileNo: 9876543210,
      video: <DownloadIcon />,
      action: <EventAvailableIcon />,
    },
  ],
};

export default dataHireFreshers;
