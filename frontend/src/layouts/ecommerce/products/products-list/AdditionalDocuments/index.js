import React, {  useState } from "react";
import { Link } from "react-router-dom";
import { styled } from '@mui/material/styles';
import {
  Card,
  CardHeader,
  CardMedia,

  IconButton,

  Stack,
  Dialog,
  DialogActions,
  DialogContent,
  InputAdornment,
  TextField,
//   Tooltip,
//   Menu,
//   MenuItem,
  Autocomplete,
  Grid
} from '@mui/material';
import MoreVertIcon from '@mui/icons-material/MoreVert';
// import { red } from '@mui/material/colors';
import { FilterList, Search,  } from '@mui/icons-material';
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";
import SoftButton from "components/SoftButton";
import SoftInputDateTime from "../activeJobs/SoftInputDateTime";
import AI from "../../../../../assets/images/ai.jpg";
const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));

function AdditionalDocumentsList() {

  const [open, setOpen] = useState(false);
  const [startDate, setStartDate] = useState(null);


  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose1 = () => {
    setOpen(false);
  };

  const handleApply = () => {
    setOpen(false);
  };

  const top100Films = [
    { label: 'ABCD542S', year: 1994 },
    { label: 'ABCD562S', year: 1972 },
    { label: 'ABCD5424D', year: 1974 },
  ];

  const clientdrop = [
    { label: 'Bala', year: 1994 },
    { label: 'Vadivel', year: 1972 },
    { label: 'Prabha', year: 1974 },
  ];

  const statusdropdown = [
    { label: 'Active', year: 1994 },
    { label: 'In Active', year: 1972 },
    { label: 'Completed', year: 1974 },
  ];

  return (
    <SoftBox sx={{ marginTop: "40px" }}>
      <SoftBox display="flex" justifyContent="space-between" alignItems="flex-start" p={3}>
        <SoftBox display="flex" alignItems="center" lineHeight={1}>
          <SoftTypography variant="h5" fontWeight="medium">
            Documents
          </SoftTypography>
        </SoftBox>
        <Stack spacing={1} direction="row">
          <TextField
            variant="outlined"
            size="small"
            placeholder="Search"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <IconButton>
                    <Search />
                  </IconButton>
                </InputAdornment>
              ),
            }}
            sx={{
              ".css-15v2jta-MuiInputBase-root-MuiOutlinedInput-root": {
                height: "46px !important",
              },
              marginRight: 1,
            }}
          />
          <SoftButton variant="gradient" color="info" size="small" sx={{ marginRight: 1, height: "46px" }} startIcon={<FilterList />} onClick={handleClickOpen}>
            Filter
          </SoftButton>
          <Dialog open={open} onClose={handleClose1} maxWidth="md">
            <DialogContent sx={{ width: 500 }}>
              <div style={{ display: 'flex', flexDirection: 'column', gap: '20px', padding: "47px" }}>
                <div style={{ display: 'flex', alignItems: 'center' }}>
                  <SoftTypography style={{ marginRight: '20px', minWidth: '120px', fontSize: "1rem" }}>Date Range</SoftTypography>
                  <SoftInputDateTime
                    value={startDate}
                    onChange={(e) => setStartDate(e.target.value)}
                    sx={{ width: "500px !important" }}
                  />
                </div>
                <div style={{ display: 'flex', alignItems: 'center' }}>
                  <SoftTypography style={{ marginRight: '20px', minWidth: '120px', fontSize: "1rem" }}>Job ID</SoftTypography>
                  <Autocomplete
                    disablePortal
                    id="name-autocomplete"
                    options={top100Films}
                    sx={{ width: '100%' }}
                    renderInput={(params) => <TextField {...params} placeholder="Job ID" />}
                  />
                </div>
                <div style={{ display: 'flex', alignItems: 'center' }}>
                  <SoftTypography style={{ marginRight: '20px', minWidth: '120px', fontSize: "1rem" }}>Client</SoftTypography>
                  <Autocomplete
                    disablePortal
                    id="age-autocomplete"
                    options={clientdrop}
                    sx={{ width: '100%' }}
                    renderInput={(params) => <TextField {...params} placeholder="Client" />}
                  />
                </div>
                <div style={{ display: 'flex', alignItems: 'center' }}>
                  <SoftTypography style={{ marginRight: '20px', minWidth: '120px', fontSize: "1rem" }}>Status</SoftTypography>
                  <Autocomplete
                    disablePortal
                    id="location-autocomplete"
                    options={statusdropdown}
                    sx={{ width: '100%' }}
                    renderInput={(params) => <TextField {...params} placeholder="Status" />}
                  />
                </div>
              </div>
            </DialogContent>
            <DialogActions>
              <SoftButton variant="gradient" color="info" onClick={handleClose1}>
                Reset
              </SoftButton>
              <SoftButton variant="gradient" color="info" onClick={handleApply}>
                Apply
              </SoftButton>
            </DialogActions>
          </Dialog>
          <Link to="#">
            <SoftButton variant="gradient" color="info" size="small" sx={{ height: "46px" }}>
              + New Job
            </SoftButton>
          </Link>
        </Stack>
      </SoftBox>
      <Grid sx={{display:"flex"}}>
      <Card sx={{ maxWidth: 345}}>
        
        <CardMedia
          component="img"
          height="194"
          image={AI}
          alt="Paella dish"
        />
        <CardHeader
         
         action={
           <IconButton aria-label="settings">
             <MoreVertIcon />
           </IconButton>
         }
         title="Shrimp and Chorizo Paella"
         subheader="September 14, 2016"
       />
      </Card>
      <Card sx={{ maxWidth: 345 , marginLeft:"20px"}}>
        
        <CardMedia
          component="img"
          height="194"
          image={AI}
          alt="Paella dish"
        />
        <CardHeader
         
         action={
           <IconButton aria-label="settings">
             <MoreVertIcon />
           </IconButton>
         }
         title="Shrimp and Chorizo Paella"
         subheader="September 14, 2016"
       />
       
      </Card>
      </Grid>
    </SoftBox>
  );
}

export default AdditionalDocumentsList;
