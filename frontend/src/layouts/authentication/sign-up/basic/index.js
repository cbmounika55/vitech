import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import Card from "@mui/material/Card";
import Checkbox from "@mui/material/Checkbox";
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";
import SoftInput from "components/SoftInput";
import SoftButton from "components/SoftButton";
import BasicLayout from "layouts/authentication/components/BasicLayout";
import Socials from "layouts/authentication/components/Socials";
import Separator from "layouts/authentication/components/Separator";
import curved6 from "assets/images/curved-images/curved6.jpg";
import axios from "axios";
import { useDispatch } from "react-redux";
import { setUser } from "../../../../actions/useActions";

function Basic() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [agreement, setAgreement] = useState(true);
  const [errors, setErrors] = useState({
    name: "",
    email: "",
    password: "",
    phoneNumber: "",
    otp: "",
  });
  const [phoneNumber, setPhoneNumber] = useState("");
  const [otp, setOtp] = useState("");
  const [isOtpSent, setIsOtpSent] = useState(false);
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const validatePhoneNumber = (phoneNumber) => {
    const re = /^[0-9\b]+$/;
    return re.test(String(phoneNumber)) && phoneNumber.length === 10;
  };
  const phoneNumberError = validatePhoneNumber(phoneNumber) ? "" : "Invalid phone number";

  const handleSetAgreement = () => setAgreement(!agreement);

  const validateEmail = (email) => {
    const re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return re.test(String(email).toLowerCase());
  };

  const validatePassword = (password) => {
    const re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
    return re.test(password);
  };

  const handleSubmit = () => {
    const nameError = name ? "" : "Name is required";
    const emailError = validateEmail(email) ? "" : "Invalid email address";
    const passwordError = validatePassword(password)
      ? ""
      : "Password must be at least 8 characters long, contain at least one uppercase letter, one lowercase letter, one number, and one special character";

    setErrors({
      name: nameError,
      email: emailError,
      password: passwordError,
      phoneNumber: phoneNumberError,
    });

    if (!nameError && !emailError && !passwordError && !phoneNumberError) {
      axios
        .post("http://localhost:5000/register", { name, email, password, phoneNumber })
        .then((result) => {
          setIsOtpSent(true);
        })
        .catch((err) => {
          console.error(err);
          if (err.response && err.response.data && err.response.data.error) {
            setErrors((prevErrors) => ({
              ...prevErrors,
              server: err.response.data.error,
            }));
          }
        });
    }
  };

  const handleVerifyOtp = () => {
    axios
      .post("http://localhost:5000/validate", { email, otp })
      .then((result) => {
        if (result.data.success) {
          console.log("Email verified successfully");
          dispatch(setUser(name, email));
          navigate("/dashboards");
        } else {
          console.error("OTP verification failed: ", result.data.error);
          setErrors((prevErrors) => ({
            ...prevErrors,
            otp: "Invalid OTP",
          }));
        }
      })
      .catch((err) => {
        console.error("OTP verification request failed: ", err);
      });
  };

  return (
    <BasicLayout
      title="Welcome!"
      description="Use these awesome forms to login or create new account in your project for free."
      image={curved6}
    >
      <Card>
        <SoftBox p={3} mb={1} textAlign="center">
          <SoftTypography variant="h5" fontWeight="medium">
            Register with
          </SoftTypography>
        </SoftBox>
        <SoftBox mb={2}>
          <Socials />
        </SoftBox>
        <Separator />
        <SoftBox pt={2} pb={3} px={3}>
          {!isOtpSent ? (
            <SoftBox component="form" role="form">
              <SoftBox mb={2}>
                <SoftInput
                  placeholder="Name"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                />
                {errors.name && (
                  <SoftTypography color="error" variant="caption">
                    {errors.name}
                  </SoftTypography>
                )}
              </SoftBox>
              <SoftBox mb={2}>
                <SoftInput
                  type="email"
                  placeholder="Email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
                {errors.email && (
                  <SoftTypography color="error" variant="caption">
                    {errors.email}
                  </SoftTypography>
                )}
              </SoftBox>
              <SoftBox mb={2}>
                <SoftInput
                  type="password"
                  placeholder="Password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
                {errors.password && (
                  <SoftTypography color="error" variant="caption">
                    {errors.password}
                  </SoftTypography>
                )}
              </SoftBox>
              <SoftBox mb={2}>
                <SoftInput
                  type="number"
                  placeholder="Enter your Phone number"
                  value={phoneNumber}
                  onChange={(e) => setPhoneNumber(e.target.value)}
                />
                {errors.phoneNumber && (
                  <SoftTypography color="error" variant="caption">
                    {errors.phoneNumber}
                  </SoftTypography>
                )}
              </SoftBox>
              <SoftBox display="flex" alignItems="center">
                <Checkbox checked={agreement} onChange={handleSetAgreement} />
                <SoftTypography
                  variant="button"
                  fontWeight="regular"
                  onClick={handleSetAgreement}
                  sx={{ cursor: "pointer", userSelect: "none" }}
                >
                  &nbsp;&nbsp;I agree to the&nbsp;
                </SoftTypography>
                <SoftTypography
                  component="a"
                  href="#"
                  variant="button"
                  fontWeight="bold"
                  textGradient
                >
                  Terms and Conditions
                </SoftTypography>
              </SoftBox>
              <SoftBox mt={4} mb={1}>
                <SoftButton variant="gradient" color="dark" fullWidth onClick={handleSubmit}>
                  sign up
                </SoftButton>
              </SoftBox>
            </SoftBox>
          ) : (
            <SoftBox component="form" role="form">
              <SoftBox mb={2}>
                <SoftInput
                  placeholder="Enter OTP"
                  value={otp}
                  onChange={(e) => setOtp(e.target.value)}
                />
                {errors.otp && (
                  <SoftTypography color="error" variant="caption">
                    {errors.otp}
                  </SoftTypography>
                )}
              </SoftBox>
              <SoftBox mt={4} mb={1}>
                <SoftButton variant="gradient" color="dark" fullWidth onClick={handleVerifyOtp}>
                  Verify OTP
                </SoftButton>
              </SoftBox>
            </SoftBox>
          )}
          <SoftBox mt={3} textAlign="center">
            <SoftTypography variant="button" color="text" fontWeight="regular">
              Already have an account?&nbsp;
              <SoftTypography
                component={Link}
                to="/sign-in"
                variant="button"
                color="dark"
                fontWeight="bold"
                textGradient
              >
                Sign in
              </SoftTypography>
            </SoftTypography>
          </SoftBox>
        </SoftBox>
      </Card>
    </BasicLayout>
  );
}

export default Basic;
