// /**
// =========================================================
// * Soft UI Dashboard PRO- v4.0.2
// =========================================================

// * Product Page: https://www.creative-tim.com/product/soft-ui-dashboard-pro-react
// * Copyright 2023 Creative Tim (https://www.creative-tim.com)

// Coded by www.creative-tim.com

//  =========================================================

// * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// */

// import { useState } from "react";

// // react-router-dom components
// import { Link } from "react-router-dom";

// // @mui material components
// import Checkbox from "@mui/material/Checkbox";

// // Soft UI Dashboard PROcomponents
// import SoftBox from "components/SoftBox";
// import SoftTypography from "components/SoftTypography";
// import SoftInput from "components/SoftInput";
// import SoftButton from "components/SoftButton";

// // Authentication layout components
// import IllustrationLayout from "layouts/authentication/components/IllustrationLayout";

// // Images
// import rocket from "assets/images/illustrations/rocket-white.png";

// function Illustration() {
//   const [agreement, setAgreemnet] = useState(true);

//   const handleSetAgremment = () => setAgreemnet(!agreement);

//   return (
//     <IllustrationLayout
//       title="Sign Up"
//       description="Enter your email and password to register"
//       illustration={{
//         image: rocket,
//         title: "Your journey starts here",
//         description:
//           "Just as it takes a company to sustain a product, it takes a community to sustain a protocol.",
//       }}
//     >
//       <SoftBox component="form" role="form">
//         <SoftBox mb={2}>
//           <SoftInput placeholder="Name" size="large" />
//         </SoftBox>
//         <SoftBox mb={2}>
//           <SoftInput type="email" placeholder="Email" size="large" />
//         </SoftBox>
//         <SoftBox mb={2}>
//           <SoftInput type="password" placeholder="Password" size="large" />
//         </SoftBox>
//         <SoftBox display="flex" alignItems="center">
//           <Checkbox checked={agreement} onChange={handleSetAgremment} />
//           <SoftTypography
//             variant="button"
//             fontWeight="regular"
//             onClick={handleSetAgremment}
//             sx={{ cursor: "pointer", userSelect: "none" }}
//           >
//             &nbsp;&nbsp;I agree the&nbsp;
//           </SoftTypography>
//           <SoftTypography component="a" href="#" variant="button" fontWeight="bold" textGradient>
//             Terms and Conditions
//           </SoftTypography>
//         </SoftBox>
//         <SoftBox mt={4} mb={1}>
//           <SoftButton variant="gradient" color="info" size="large" fullWidth>
//             sign up
//           </SoftButton>
//         </SoftBox>
//         <SoftBox mt={3} textAlign="center">
//           <SoftTypography variant="button" color="text" fontWeight="regular">
//             Already have an account?&nbsp;
//             <SoftTypography
//               component={Link}
//               to="/authentication/sign-in/illustration"
//               variant="button"
//               color="info"
//               fontWeight="bold"
//               textGradient
//             >
//               Sign in
//             </SoftTypography>
//           </SoftTypography>
//         </SoftBox>
//       </SoftBox>
//     </IllustrationLayout>
//   );
// }

// export default Illustration;
/**
=========================================================
* Soft UI Dashboard PRO React - v4.0.2
=========================================================

* Product Page: https://www.creative-tim.com/product/soft-ui-dashboard-pro-react
* Copyright 2023 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

import { useState } from "react";

// react-router-dom components
// import { Link } from "react-router-dom";

// @mui material components
// import Checkbox from "@mui/material/Checkbox";

// Soft UI Dashboard PRO React components
import SoftBox from "components/SoftBox";
import SoftInput from "components/SoftInput";
import SoftButton from "components/SoftButton";
import SoftSelect from "components/SoftSelect";
import selectData from "layouts/pages/account/settings/components/BasicInfo/data/selectData";
// Authentication layout components
import IllustrationLayout from "layouts/authentication/components/IllustrationLayout";
import SoftInputDateTime from "layouts/ecommerce/products/products-list/activeJobs/SoftInputDateTime";
import ReCAPTCHA from "react-google-recaptcha";
import axios from "axios";
import rocket from "assets/images/illustrations/rocket-white.png";
import "./illustration.css";
// Images

function Illustration() {
  const [agreement, setAgreement] = useState(true);
  const [updates, setUpdates] = useState(false);
  const [linkDate, setLinkDate] = useState("");
  const [recaptchaVerified, setRecaptchaVerified] = useState(false);
  const [formData, setFormData] = useState({
    name: "",
    org_email: "",
    organisation: "",
    phone: "",
    message: "",
    date: "",
  });

  const handleRecaptchaChange = (value) => {
    setRecaptchaVerified(!!value);
  };

  const handleSetAgreement = () => setAgreement(!agreement);
  const handleSetUpdates = () => setUpdates(!updates);

  const handleChange = (e) => {
    const { name, value } = e.target;
    console.log(name, value, "post");
    setFormData({ ...formData, [name]: value });
    //setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!recaptchaVerified) return;

    try {
      // Log formData to console
      console.log("Form Data:", formData);

      await axios.post("http://localhost:3001/send-email", {
        ...formData,
        date: linkDate,
      });
      alert("Request sent successfully");
    } catch (error) {
      console.error("Error sending request", error);
      alert("Failed to send request");
    }
  };

  return (
    <IllustrationLayout
      illustration={{
        image: rocket,
        title: "Your journey starts here",
        description:
          "Just as it takes a company to sustain a product, it takes a community to sustain a protocol.",
      }}
    >
      <div className="illustration-container">
        <h3 style={{ marginTop: "20px" }}>Book a Demo</h3>
        <h5> We are here for you! how can we help?</h5>

        <SoftBox component="form" role="form" onSubmit={handleSubmit}>
          <SoftBox mb={2} mt={4}>
            <SoftInputDateTime
              label="Select Date and Time"
              value={linkDate}
              name="date"
              onChange={(e) => {
                handleChange(e);
                setLinkDate(e.target.value); // Update linkDate when the date input changes
              }}
            />
          </SoftBox>

          <SoftBox mb={2}>
            <SoftInput
              placeholder="Name"
              size="large"
              name="name"
              value={formData.name}
              onChange={handleChange}
            />
          </SoftBox>
          <SoftBox mb={2}>
            <SoftSelect placeholder="Type of Support" options={selectData.typeofsupport} />
          </SoftBox>
          <SoftBox mb={2}>
            <SoftInput
              placeholder="Organisation"
              size="large"
              name="organisation"
              value={formData.organisation}
              onChange={handleChange}
            />
          </SoftBox>
          <SoftBox mb={2}>
            <SoftInput
              placeholder="Organisation Email ID"
              size="large"
              name="org_email"
              value={formData.org_email}
              onChange={handleChange}
            />
          </SoftBox>

          <SoftBox mb={2}>
            <SoftInput
              placeholder="Phone no"
              size="large"
              name="phone"
              value={formData.phone}
              onChange={handleChange}
            />
          </SoftBox>
          <SoftBox mb={2}>
            <SoftInput
              placeholder="Type here..."
              multiline
              rows={5}
              name="message"
              value={formData.message}
              onChange={handleChange}
            />
          </SoftBox>
          {/* <SoftBox display="flex" alignItems="center" mt={2}>
          <Checkbox checked={updates} onChange={handleSetUpdates} />
          <SoftTypography
            variant="button"
            fontWeight="regular"
            onClick={handleSetUpdates}
            sx={{ cursor: "pointer", userSelect: "none" }}
          >
            &nbsp;&nbsp;Send me updates from Evalgator
          </SoftTypography>
        </SoftBox> */}
          {/* <SoftBox mt={4} mb={1}>
          <SoftButton variant="gradient" color="info" size="large" fullWidth>
            send request
          </SoftButton>
        </SoftBox> */}
          <SoftBox mt={2} mb={1} display="flex" justifyContent="center">
            <ReCAPTCHA
              sitekey="6LdvJOkpAAAAAM5MkhEtKio6Vz_zph7qMelagXZt"
              onChange={handleRecaptchaChange}
            />
          </SoftBox>
          {/* <SoftBox mt={2} mb={1} display="flex" justifyContent="center">
          <ReCAPTCHA
            sitekey="6LdvJOkpAAAAAM5MkhEtKio6Vz_zph7qMelagXZt"
            onChange={handleRecaptchaChange}
          />
        </SoftBox> */}
          <SoftBox mt={4} mb={1}>
            <SoftButton
              variant="gradient"
              color="info"
              size="large"
              fullWidth
              type="submit"
              disabled={!recaptchaVerified}
            >
              send request
            </SoftButton>
          </SoftBox>
        </SoftBox>
      </div>
    </IllustrationLayout>
  );
}

export default Illustration;
