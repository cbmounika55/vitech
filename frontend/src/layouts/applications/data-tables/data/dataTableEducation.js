// import ActionCell from "layouts/ecommerce/products/products-list/components/ActionCell";

const dataTableEducation = {
  columns: [
    { Header: "Institution name", accessor: "institutionName", width: "20%" },
    { Header: "from date", accessor: "fromDate" },
    { Header: "to date", accessor: "toDate" },
    { Header: "action", accessor: "action" },
  ],

  rows: [
    // {
    //   institutionName: "Image Creative Education",
    //   fromDate: "4/11/2021",
    //   toDate: "4/2/2024",
    //   action: <ActionCell />,
    // },
    // {
    //   institutionName: "Lara Puleque",
    //   fromDate: "8/2/2021",
    //   toDate: "8/2/2023",
    //   action: <ActionCell />,
    // },
  ],
};

export default dataTableEducation;
