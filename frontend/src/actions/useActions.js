export const SET_USER = 'SET_USER';
export const LOGOUT = 'LOGOUT';

export const setUser = (name, email) => ({
  type: SET_USER,
  payload: { name, email },
});

export const logout = () => ({
  type: LOGOUT,
});