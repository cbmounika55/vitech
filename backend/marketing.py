from flask import Flask, request, jsonify
from flask_cors import CORS
from pymongo import MongoClient
from urllib.parse import quote_plus

app = Flask(__name__)
CORS(app)

# MongoDB credentials
username = quote_plus('CB2024')
password = quote_plus('Cab123')

# Connect to MongoDB Atlas
client = MongoClient(f'mongodb+srv://{username}:{password}@virtualai.c4wot6c.mongodb.net/?retryWrites=true&w=majority')

# Database name
db = client['AgencyProfile']

# Collection name for marketing information
marketing_collection = db['marketing']

@app.route('/api/marketing_info', methods=['POST'])
def save_marketing_info():
    try:
        data = request.get_json()
        marketing_collection.insert_one(data)
        return jsonify({'message': 'Marketing information saved successfully'}), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 500

@app.route('/api/get_marketing_info', methods=['GET'])
def get_marketing_info():
    try:
        marketing_info = list(marketing_collection.find())
        return jsonify(marketing_info), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 500

if __name__ == '__main__':
    app.run(debug=True)
