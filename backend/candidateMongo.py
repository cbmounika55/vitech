from flask import Flask, request, jsonify
from pymongo import MongoClient
from urllib.parse import quote_plus
from flask_cors import CORS
import uuid

app = Flask(__name__)
CORS(app)

# MongoDB credentials
username = quote_plus('CB2024')
password = quote_plus('Cab123')

# Connect to MongoDB Atlas
client = MongoClient(f'mongodb+srv://{username}:{password}@virtualai.c4wot6c.mongodb.net/?retryWrites=true&w=majority')

# Database name
db = client['Canditate']
# Collection name for candidate information
candidate_collection = db['canditateinfo']

@app.route('/save_candidate', methods=['POST'])
def save_candidate():
    data = request.json
    if data:
        # Generate a unique candidate ID
        candidate_id = str(uuid.uuid4())
        # Add the candidate ID to the data
        data['candidate_id'] = candidate_id
        # Save the data to the MongoDB collection
        try:
            result = candidate_collection.insert_one(data)
            print(f"Inserted document ID: {result.inserted_id}")
            return jsonify({"message": "Candidate saved successfully", "candidate_id": candidate_id}), 201
        except Exception as e:
            print(f"Error inserting document: {e}")
            return jsonify({"message": "Failed to save candidate"}), 500
    else:
        return jsonify({"message": "No data provided"}), 400

if __name__ == '__main__':
    app.run(debug=True, port=5000)  # Ensure the port matches the Axios request
