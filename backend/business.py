from flask import Flask, request, jsonify
from flask_cors import CORS
from pymongo import MongoClient
from urllib.parse import quote_plus

# MongoDB credentials
username = quote_plus('CB2024')
password = quote_plus('Cab123')

# Connect to MongoDB Atlas
client = MongoClient(f'mongodb+srv://{username}:{password}@virtualai.c4wot6c.mongodb.net/?retryWrites=true&w=majority')
db = client["AgencyProfile"]
collection = db["business"]

app = Flask(__name__)
CORS(app)  # Enable CORS for all routes

@app.route('/api/business', methods=['POST', 'OPTIONS'])  # Change the route to '/api/business'
def save_data():
    if request.method == 'OPTIONS':
        return '', 200  # Respond with an empty string for OPTIONS request

    data = request.json
    if not data:
        return jsonify({"error": "No data provided"}), 400

    try:
        collection.insert_one(data)
        return jsonify({"message": "Data saved successfully"}), 201
    except Exception as e:
        return jsonify({"error": str(e)}), 500


if __name__ == '__main__':
    app.run(port=5000, debug=True)
