from flask import Flask, jsonify, request
from flask_cors import CORS
from pymongo import MongoClient

app = Flask(__name__)
CORS(app)  # Enable CORS for all routes

# MongoDB connection setup
client = MongoClient('mongodb+srv://CB2024:Cab123@virtualai.c4wot6c.mongodb.net/?retryWrites=true&w=majority')
db = client["AgencyProfile"]
collection = db["contact_info"]

# Define your routes here
@app.route('/api/contact', methods=['POST'])
def save_contact():
    data = request.json
    try:
        # Insert the contact information into the MongoDB collection
        collection.insert_one(data)
        return jsonify({'message': 'Contact information saved successfully'})
    except Exception as e:
        return jsonify({'error': str(e)}), 500

if __name__ == '__main__':
    app.run(debug=True)
