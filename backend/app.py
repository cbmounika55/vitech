import os
import urllib.parse
from pymongo import MongoClient
import re
import json
from flask import Flask, request, jsonify
from flask_cors import CORS
from werkzeug.utils import secure_filename
from pdfminer.high_level import extract_text
import spacy
from spacy.matcher import Matcher
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail 
# from bs4 import BeautifulSoup
app = Flask(__name__)
CORS(app)  # Enable Cross-Origin Resource Sharing
SENDGRID_API_KEY = "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"
# Environment variables or default values for MongoDB credentials
# username = os.getenv('MONGO_USERNAME', 'cbadmin')
# password = os.getenv('MONGO_PASSWORD', 'cab@123')

# # Escape username and password for use in a URL
# escaped_username = urllib.parse.quote_plus(username)

# escaped_password = urllib.parse.quote_plus(password)

# MongoDB connection string
connection_string = "mongodb+srv://CB2024:Cab123@virtualai.c4wot6c.mongodb.net/?retryWrites=true&w=majority&appName=VirtualAI"

# Initialize the MongoDB client
client = MongoClient(connection_string)
db = client["ViTech"]
collection = db["int_schedule"]

question_db = client["InterviewQuestions"]




@app.route('/api/schedule', methods=['POST'])
def save_schedule():
    try:
        schedule_data = request.json
        result = collection.insert_one(schedule_data)
        inserted_id = str(result.inserted_id)
        
        # Send email notification
        send_email([schedule_data["candidate_email"]], schedule_data)
        
        return jsonify({"message": "Schedule data saved successfully", "inserted_id": inserted_id}), 201
    except Exception as e:
        return jsonify({"error": str(e)}), 500





def send_email(recipient_emails, data):
    from_email = 'guberanrealme@gmail.com'
    subject = "Invitation to Interview"
    content = f"""\
  <!DOCTYPE html>
<html>
<head>
    <title>Interview Schedule</title>
</head>
<body>
    <div>
        <h1>Interview Schedule Notification</h1>
        <p>Dear {data['Candidate_id']},</p>
        <p>We are pleased to inform you that you have been shortlisted for the next round of interviews for the <strong>Python Developer</strong> position at [Company Name]. Below are the details of your interview schedule:</p>
        <table>
            <tr>
                <td><strong>Date:</strong></td>
                <td>{data['startDate']}</td>
            </tr>
            <tr>
                <td><strong>Time:</strong></td>
                <td>{data['InterviewTime']}</td>
            </tr>
           
            <tr>
                <td><strong>Interview Type:</strong></td>
                <td>Remote</td>
            </tr>
        </table>
        <p>If the interview is remote, please use the following link to join the session:</p>
        <p><a href="{data['meet_id']}">Join Interview</a></p>
        <p>Please ensure that you are available and prepared at the scheduled time. If you need to reschedule, contact us at least 48 hours in advance.</p>
        <p>We look forward to speaking with you.</p>
        <p>Best regards,</p>
        <p>VI technologies</p>
        <p>&copy; 2024 VI technologies. All rights reserved.</p>
    </div>
</body>
</html>

    """

    message = Mail(
        from_email=from_email,
        to_emails=recipient_emails,
        subject=subject,
        html_content=content
    )

    try:
        sg = SendGridAPIClient(SENDGRID_API_KEY)
        response = sg.send(message)
        print(f"Email sent! Status Code: {response.status_code}, Response Body: {response.body}")
    except Exception as e:
        print(f"Failed to send email: {str(e)}")

@app.route('/api/get_schedule', methods=['GET'])
def get_schedules():
    try:
        documents = collection.find()  # Retrieve all documents
        schedules = []
        for doc in documents:
            doc['_id'] = str(doc['_id'])  # Convert ObjectId to string
            schedules.append(doc)

        return jsonify(schedules), 200
    except Exception as e:
        return jsonify({"error": str(e)}), 500
    
# @app.route('/api/get_schedule/<cand_id>', methods=['GET'])
# def get_schedules(cand_id):
#     try:
#         print(cand_id)
#         schedules = list(collection.find())
#         for schedule in schedules:
#              schedule["_id"] = str(schedule["_id"])
#         filtered_list = [item for item in schedules if item["Candidate_id"] == cand_id]
#         print(filtered_list,"test")   
            
#         return jsonify(filtered_list), 200
#     except Exception as e:
#         return jsonify({"error": str(e)}), 500

    
    
    
@app.route('/api/get_questions/<primary_skills>', methods=['GET'])
def get_questions(primary_skills):
    try:
        print(primary_skills)
        additional_data_collection = question_db[primary_skills]
        documents = additional_data_collection.find()  # Retrieve all documents
        schedules = []
        for doc in documents:
            doc['_id'] = str(doc['_id'])  # Convert ObjectId to string
            schedules.append(doc)

        return jsonify(schedules), 200
    except Exception as e:
        return jsonify({"error": str(e)}), 500

# Call the function to retrieve and print all documents
# get_all_documents()
    
UPLOAD_FOLDER = './uploads'
if not os.path.exists(UPLOAD_FOLDER):
    os.makedirs(UPLOAD_FOLDER)

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def extract_text_from_pdf(pdf_path):
    return extract_text(pdf_path)

def extract_contact_number_from_resume(text):
    contact_number = None
    pattern = r"\b(?:\+?\d{1,3}[-.\s]?)?\(?\d{3}\)?[-.\s]?\d{3}[-.\s]?\d{4}\b"
    match = re.search(pattern, text)
    if match:
        contact_number = match.group()
    return contact_number

def extract_email_from_resume(text):
    email = None
    pattern = r"\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}\b"
    match = re.search(pattern, text)
    if match:
        email = match.group()
    return email

def extract_skills_from_resume(text, skills_list):
    skills = []
    for skill in skills_list:
        pattern = r"\b{}\b".format(re.escape(skill))
        match = re.search(pattern, text, re.IGNORECASE)
        if match:
            skills.append(skill)
    return skills

def extract_education_from_resume(text):
    education = []
    pattern = r"(?i)(?:Bsc|\bB\.\w+|\bM\.\w+|\bPh\.D\.\w+|\bBachelor(?:'s)?|\bMaster(?:'s)?|\bPh\.D)\s(?:\w+\s)*\w+"
    matches = re.findall(pattern, text)
    for match in matches:
        education.append(match.strip())
    return education

def extract_name(resume_text):
    nlp = spacy.load('en_core_web_sm')
    matcher = Matcher(nlp.vocab)
    patterns = [
        [{'POS': 'PROPN'}, {'POS': 'PROPN'}],
        [{'POS': 'PROPN'}, {'POS': 'PROPN'}, {'POS': 'PROPN'}],
        [{'POS': 'PROPN'}, {'POS': 'PROPN'}, {'POS': 'PROPN'}, {'POS': 'PROPN'}]
    ]
    for pattern in patterns:
        matcher.add('NAME', [pattern])
    doc = nlp(resume_text)
    matches = matcher(doc)
    for match_id, start, end in matches:
        span = doc[start:end]
        return span.text
    return None

def extract_cities_from_resume(text, cities_list):
    cities = []
    for city in cities_list:
        pattern = r"\b{}\b".format(re.escape(city))
        match = re.search(pattern, text, re.IGNORECASE)
        if match:
            cities.append(city)
    return cities

def extract_hobbies_from_resume(text, hobbies_list):
    hobbies = []
    for hobby in hobbies_list:
        pattern = r"\b{}\b".format(re.escape(hobby))
        match = re.search(pattern, text, re.IGNORECASE)
        if match:
            hobbies.append(hobby)
    return hobbies

def extract_states_from_resume(text, indian_states_list):
    states = []
    for state in indian_states_list:
        pattern = r"\b{}\b".format(re.escape(state))
        match = re.search(pattern, text, re.IGNORECASE)
        if match:
            states.append(state)
    return states

def extract_linkedin_info_from_resume(text):
    linkedin_profile_ids = []
    linkedin_profile_urls = []
    id_pattern = r'(linkedin\.com\/in\/[a-zA-Z0-9_-]+)'
    id_matches = re.findall(id_pattern, text)
    linkedin_profile_ids.extend(id_matches)
    url_pattern = r'(https?:\/\/(?:www\.)?linkedin\.com\/in\/[a-zA-Z0-9_-]+)'
    url_matches = re.findall(url_pattern, text)
    linkedin_profile_urls.extend(url_matches)
    return linkedin_profile_ids, linkedin_profile_urls

@app.route('/upload', methods=['POST'])
def upload_file():
    if 'file' not in request.files:
        return jsonify({'message': 'No file part'}), 400

    file = request.files['file']
    if file.filename == '':
        return jsonify({'message': 'No selected file'}), 400

    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        file.save(file_path)

        # Parse the resume
        parsed_data = parse_resume(file_path)
        return jsonify({'message': 'File successfully uploaded', 'filename': filename, 'parsed_data': parsed_data}), 200

    return jsonify({'message': 'File type not allowed'}), 400

def allowed_file(filename):
    allowed_extensions = {'pdf'}
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in allowed_extensions

def parse_resume(file_path):
    text = extract_text_from_pdf(file_path)

    skills_list = ['Python', 'Data Analysis', 'Machine Learning', 'Communication', 'Project Management', 'Deep Learning', 'SQL', 'Tableau']
    cities_list = ['Hyderabad', 'Mumbai', 'Bangalore', 'Chennai', 'Kolkata', 'Delhi']
    hobbies_list = ['drawing', 'painting', 'sketching', 'photography']
    indian_states_list = ['Andhra Pradesh', 'Arunachal Pradesh', 'Assam', 'Bihar', 'Chhattisgarh', 'Goa', 'Gujarat', 'Haryana', 'Himachal Pradesh', 'Jharkhand', 'Karnataka', 'Kerala', 'Madhya Pradesh', 'Maharashtra', 'Manipur', 'Meghalaya', 'Mizoram', 'Nagaland', 'Odisha', 'Punjab', 'Rajasthan', 'Sikkim', 'Tamil Nadu', 'Telangana', 'Tripura', 'Uttar Pradesh', 'Uttarakhand', 'West Bengal', 'Andaman and Nicobar Islands', 'Chandigarh', 'Dadra and Nagar Haveli and Daman and Diu', 'Lakshadweep', 'Delhi', 'Puducherry']

    parsed_data = {
        'name': extract_name(text),
        'contact_number': extract_contact_number_from_resume(text),
        'email': extract_email_from_resume(text),
        'skills': extract_skills_from_resume(text, skills_list),
        'education': extract_education_from_resume(text),
        'cities': extract_cities_from_resume(text, cities_list),
        'hobbies': extract_hobbies_from_resume(text, hobbies_list),
        'indian_states': extract_states_from_resume(text, indian_states_list),
        'linkedin_ids': extract_linkedin_info_from_resume(text)[0],
        'linkedin_urls': extract_linkedin_info_from_resume(text)[1]
    }

    return parsed_data

if __name__ == '__main__':
    app.run(debug=True)
