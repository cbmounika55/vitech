from flask import Flask, request, jsonify
from flask_cors import CORS
from pymongo import MongoClient
from urllib.parse import quote_plus

app = Flask(__name__)
CORS(app)

# MongoDB credentials
username = quote_plus('CB2024')
password = quote_plus('Cab123')

# Connect to MongoDB Atlas
client = MongoClient(f'mongodb+srv://{username}:{password}@virtualai.c4wot6c.mongodb.net/?retryWrites=true&w=majority')

# Database name
db = client['AgencyProfile']

# Collection name for additional documents
additional_doc_collection = db['additonaldoc']

@app.route('/api/additionaldoc', methods=['POST'])
def upload_additional_documents():
    try:
        file = request.files['file']
        name = request.form['name']

        # Save the file to MongoDB GridFS
        file_id = additional_doc_collection.insert_one({'name': name, 'file': file.read()}).inserted_id
        
        return jsonify({'message': 'Additional document saved successfully', 'file_id': str(file_id)})
    except Exception as e:
        return jsonify({'error': str(e)}), 500

if __name__ == '__main__':
    app.run(debug=True)
