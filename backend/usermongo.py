
# from flask import Flask, Blueprint, request, jsonify
# from werkzeug.security import generate_password_hash, check_password_hash
# from flask_cors import CORS, cross_origin
# from pymongo import MongoClient, errors
# from urllib.parse import quote_plus
# import random
# import string
# import sendgrid
# from sendgrid.helpers.mail import Mail
# import logging
 
# app = Flask(__name__)
# CORS(app)  # Enable CORS for all routes
 
# # MongoDB credentials
# username = quote_plus('CB2024')
# password = quote_plus('Cab123')
 
# # Connect to MongoDB Atlas
# client = MongoClient(f'mongodb+srv://{username}:{password}@virtualai.c4wot6c.mongodb.net/?retryWrites=true&w=majority')
# db = client['SignUP']  # Replace with your database name
# users_collection = db['Register']  # Replace with your collection name
 
# # SendGrid API key (replace with your own)
# SENDGRID_API_KEY = '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'
 
# # Configure logging
# logging.basicConfig(level=logging.DEBUG)
 
# def send_verification_email(email, otp):
#     try:
#         sg = sendgrid.SendGridAPIClient(api_key=SENDGRID_API_KEY)
#         from_email = 'guberanrealme@gmail.com'  # Replace with your verified sender email
#         subject = 'Email Verification'
#         content = f'Your verification code is: {otp}'
#         mail = Mail(
#             from_email=from_email,
#             to_emails=email,
#             subject=subject,
#             plain_text_content=content,
#             html_content=f'<p>Your verification code is: <strong>{otp}</strong></p>'
#         )
#         response = sg.client.mail.send.post(request_body=mail.get())
#         if response.status_code == 202:
#             logging.info(f"Email sent successfully to {email}")
#         else:
#             logging.error(f"Error sending email: {response.body}")
#             raise Exception("Failed to send verification email")
#     except Exception as e:
#         logging.error(f"Error sending verification email: {e}")
#         raise e
 
# @app.route('/register', methods=['POST'])
# def register():
#     try:
#         data = request.json
#         logging.debug(f"Received registration data: {data}")
#         name = data.get('name')
#         email = data.get('email')
#         password = data.get('password')
#         phoneNumber = data.get('phoneNumber')
 
#         if not name or not email or not password or not phoneNumber:
#             return jsonify({'error': 'Please provide name, email, password, and phone number'}), 400
 
#         if not validate_email(email):
#             return jsonify({'error': 'Invalid email format'}), 400
 
#         if not validate_phone_number(phoneNumber):
#             return jsonify({'error': 'Invalid phone number'}), 400
 
#         if not validate_password(password):
#             return jsonify({'error': 'Password must be at least 8 characters long and contain at least one uppercase letter, one lowercase letter, one number, and one special character'}), 400
 
#         if users_collection.find_one({'email': email}):
#             return jsonify({'error': 'Email address already registered'}), 400
 
#         hashed_password = generate_password_hash(password)
#         otp = ''.join(random.choices(string.ascii_uppercase + string.digits, k=6))
 
#         user = {
#             'name': name,
#             'email': email,
#             'password': hashed_password,
#             'phoneNumber': phoneNumber,
#             'otp': otp,
#             'verified': False
#         }
 
#         users_collection.insert_one(user)
#         send_verification_email(email, otp)
 
#         return jsonify({'message': 'User registered successfully. Verification email sent.'}), 201
 
#     except errors.PyMongoError as e:
#         logging.error(f"MongoDB error: {e}")
#         return jsonify({'error': 'Internal server error'}), 500
#     except Exception as e:
#         logging.error(f"Error: {e}")
#         return jsonify({'error': 'Internal server error'}), 500
 
# @app.route('/validate', methods=['POST'])
# def validate():
#     try:
#         data = request.json
#         logging.debug(f"Received OTP validation data: {data}")
#         email = data.get('email')
#         otp = data.get('otp')
 
#         user = users_collection.find_one({'email': email})
 
#         if user and user['otp'] == otp:
#             users_collection.update_one({'email': email}, {'$set': {'verified': True}})
#             return jsonify({'success': True, 'message': 'Email verified successfully'}), 200
#         else:
#             return jsonify({'success': False, 'error': 'Invalid OTP'}), 400
 
#     except errors.PyMongoError as e:
#         logging.error(f"MongoDB error: {e}")
#         return jsonify({'error': 'Internal server error'}), 500
#     except Exception as e:
#         logging.error(f"Error: {e}")
#         return jsonify({'error': 'Internal server error'}), 500
 
# @app.route('/login', methods=['GET'])
# def login():
#     email = request.args.get('email')
#     password = request.args.get('password')
 
#     if not email or not password:
#         return jsonify({'error': 'Please provide email and password'}), 400
 
#     user = users_collection.find_one({'email': email})
 
#     if user and check_password_hash(user['password'], password):
#         if not user.get('verified', False):
#             return jsonify({'error': 'Email not verified'}), 400
#         return jsonify({'message': 'Login successful', 'name': user['name'], 'email': user['email']}), 200
#     else:
#         return jsonify({'error': 'Invalid email or password'}), 400
 
# def validate_email(email):
#     import re
#     email_regex = r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$'
#     return re.match(email_regex, email) is not None
 
# def validate_password(password):
#     import re
#     password_regex = r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$'
#     return re.match(password_regex, password) is not None
 
# def validate_phone_number(phone_number):
#     import re
#     phone_regex = r'^\d{10}$'
#     return re.match(phone_regex, phone_number) is not None
 
# if __name__ == '__main__':
#     app.run(debug=True)


from flask import Flask, Blueprint, request, jsonify
from werkzeug.security import generate_password_hash, check_password_hash
from flask_cors import CORS, cross_origin
from pymongo import MongoClient, errors
from urllib.parse import quote_plus
import random
import string
import sendgrid
from sendgrid.helpers.mail import Mail
import logging
 
app = Flask(__name__)
CORS(app)  # Enable CORS for all routes
 
# MongoDB credentials
username = quote_plus('CB2024')
password = quote_plus('Cab123')
 
# Connect to MongoDB Atlas
client = MongoClient(f'mongodb+srv://{username}:{password}@virtualai.c4wot6c.mongodb.net/?retryWrites=true&w=majority')
db = client['SignUP']  # Replace with your database name
users_collection = db['Register']  # Replace with your collection name
 
# SendGrid API key (replace with your own)
SENDGRID_API_KEY = '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'
 
# Configure logging
logging.basicConfig(level=logging.DEBUG)
 
def send_verification_email(email, otp):
    try:
        sg = sendgrid.SendGridAPIClient(api_key=SENDGRID_API_KEY)
        from_email = 'guberanrealme@gmail.com'  # Replace with your verified sender email
        subject = 'Email Verification'
        content = f'Your verification code is: {otp}'
        mail = Mail(
            from_email=from_email,
            to_emails=email,
            subject=subject,
            plain_text_content=content,
            html_content=f'<p>Your verification code is: <strong>{otp}</strong></p>'
        )
        response = sg.client.mail.send.post(request_body=mail.get())
        if response.status_code == 202:
            logging.info(f"Email sent successfully to {email}")
        else:
            logging.error(f"Error sending email: {response.body}")
            raise Exception("Failed to send verification email")
    except Exception as e:
        logging.error(f"Error sending verification email: {e}")
        raise e
 
@app.route('/register', methods=['POST'])
def register():
    try:
        data = request.json
        logging.debug(f"Received registration data: {data}")
        name = data.get('name')
        email = data.get('email')
        password = data.get('password')
        phoneNumber = data.get('phoneNumber')
 
        if not name or not email or not password or not phoneNumber:
            return jsonify({'error': 'Please provide name, email, password, and phone number'}), 400
 
        if not validate_email(email):
            return jsonify({'error': 'Invalid email format'}), 400
 
        if not validate_phone_number(phoneNumber):
            return jsonify({'error': 'Invalid phone number'}), 400
 
        if not validate_password(password):
            return jsonify({'error': 'Password must be at least 8 characters long and contain at least one uppercase letter, one lowercase letter, one number, and one special character'}), 400
 
        if users_collection.find_one({'email': email}):
            return jsonify({'error': 'Email address already registered'}), 400
 
        hashed_password = generate_password_hash(password)
        otp = ''.join(random.choices(string.ascii_uppercase + string.digits, k=6))
 
        user = {
            'name': name,
            'email': email,
            'password': hashed_password,
            'phoneNumber': phoneNumber,
            'otp': otp,
            'verified': False
        }
 
        users_collection.insert_one(user)
        send_verification_email(email, otp)
 
        return jsonify({'message': 'User registered successfully. Verification email sent.'}), 201
 
    except errors.PyMongoError as e:
        logging.error(f"MongoDB error: {e}")
        return jsonify({'error': 'Internal server error'}), 500
    except Exception as e:
        logging.error(f"Error: {e}")
        return jsonify({'error': 'Internal server error'}), 500
 
@app.route('/validate', methods=['POST'])
def validate():
    try:
        data = request.json
        logging.debug(f"Received OTP validation data: {data}")
        email = data.get('email')
        otp = data.get('otp')
 
        user = users_collection.find_one({'email': email})
 
        if user and user['otp'] == otp:
            users_collection.update_one({'email': email}, {'$set': {'verified': True}})
            return jsonify({'success': True, 'message': 'Email verified successfully'}), 200
        else:
            return jsonify({'success': False, 'error': 'Invalid OTP'}), 400
 
    except errors.PyMongoError as e:
        logging.error(f"MongoDB error: {e}")
        return jsonify({'error': 'Internal server error'}), 500
    except Exception as e:
        logging.error(f"Error: {e}")
        return jsonify({'error': 'Internal server error'}), 500
 
@app.route('/login', methods=['GET'])
def login():
    email = request.args.get('email')
    password = request.args.get('password')
 
    if not email or not password:
        return jsonify({'error': 'Please provide email and password'}), 400
 
    user = users_collection.find_one({'email': email})
 
    if user and check_password_hash(user['password'], password):
        if not user.get('verified', False):
            return jsonify({'error': 'Email not verified'}), 400
        return jsonify({'message': 'Login successful', 'name': user['name'], 'email': user['email']}), 200
    else:
        return jsonify({'error': 'Invalid email or password'}), 400
 
def validate_email(email):
    import re
    email_regex = r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$'
    return re.match(email_regex, email) is not None
 
def validate_password(password):
    import re
    password_regex = r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$'
    return re.match(password_regex, password) is not None
 
def validate_phone_number(phone_number):
    import re
    phone_regex = r'^\d{10}$'
    return re.match(phone_regex, phone_number) is not None
 
if __name__ == '__main__':
    app.run(debug=True)