from flask import Flask, request, jsonify
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail 
from flask_cors import CORS, cross_origin
from dotenv import load_dotenv
import os
from pymongo import MongoClient
import secrets
from bs4 import BeautifulSoup
import base64


# Load environment variables from .env file
load_dotenv()

app = Flask(__name__)
CORS(app, resources={r"/send-email": {"origins": "http://localhost:3000"}})

# Retrieve SendGrid API key from environment variables
SENDGRID_API_KEY = os.getenv("SENDGRID_API_KEY")

# MongoDB connection
MONGO_URI = "mongodb+srv://CB2024:Cab123@virtualai.c4wot6c.mongodb.net/?retryWrites=true&w=majority&appName=VirtualAI"
DATABASE_NAME = "Book_a_Demo"
COLLECTION_NAME = "demo_mail"

# Initialize MongoDB client
client = MongoClient(MONGO_URI)
db = client[DATABASE_NAME]
collection = db[COLLECTION_NAME]

@app.route('/send-email', methods=['POST'])
@cross_origin()  # This enables CORS for this specific route
def send_email():
    data = request.json
    print(data)
    org_email = data.get('org_email')
    name = data.get('name')
    organisation = data.get('organisation')
    phone = data.get('phone')
    message = data.get('message')
    date  = data.get('date')
    from_email = 'guberanrealme@gmail.com'

    # Generate a unique meeting link
    meeting_link = generate_meeting_link()

    # Save data to MongoDB
    try:
        collection.insert_one({
            "org_email": org_email,
            "name": name,
            "organisation": organisation,
            "phone": phone,
            "message": message,
            "date": date,
            "meeting_link": meeting_link
        })
    except Exception as e:
        print(f"Error saving data to MongoDB: {str(e)}")
        return jsonify({'message': 'Error saving data to MongoDB'}), 500

  
    # URLs for SVG icons
    calendar_icon_url = 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/svgs/solid/calendar.svg'
    cog_icon_url = 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/svgs/solid/cog.svg'

    # Construct the email content with HTML
    html_content = f"""\
   <!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
  <h3>Updated Event Invitation</h3>
  <p>{from_email} Invites you to attend the ViTech Demo</p>

  <div style="display:flex;flex-direction:row">
    <div>
      <p style="color: black; font-size: 14px;">
        <!-- Event Icon SVG -->
     
         <span style="color: black; font-size: 14px;"> 
        Event Title: <span style="color: black; font-size: 18px;">ViTech Demo</span>
      </span>
 </p>
    </div>
  </div>
  
  <div style="display:flex;flex-direction:row">
    <div>
      <p style="color: black; font-size: 14px;">
        <!-- Calendar Icon SVG -->
          <span style="color: black; font-size: 14px;">Date and Time:</span>
      <span style="color: black; font-size: 14px;">{date}</span>
 </p>
    </div>
  </div>

  <div style="display:flex;flex-direction:row">
    <div>
      <p style="color: black; font-size: 14px;">
        <!-- Participant Icon SVG -->
      
      <span style="color: black; font-size: 14px;">Participants:</span>
 </p>
    </div>
  </div>
  <p style="color: black; font-size: 14px;">{from_email} (Organizer), {org_email}</p>

  <p>~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ </p>
  <div style="display:flex;flex-direction:row">
    <div>
      <p style="color: black; font-size: 14px;">
        <!-- Notes Icon SVG -->
       
      <span style="color: black; font-size: 14px;">Notes:</span> </p>
      <p style="color: black; font-size: 14px;">Join with Meet: <a href="{meeting_link}">{meeting_link}</a> Or dial: (US)+1 620-712-2535 PIN: 310073840#<br>Please do not edit this section.<br>
  <p>~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ </p>

 


  <p>Thank You.</p>
</body>
</html>

    """

    # Create the email message with HTML content
    email_message = Mail(
        from_email=from_email,
        to_emails=org_email,
        subject=f'Updated Event Invitation\n{organisation} has made changes to ViTech Demo',
        html_content=html_content
    )

    # Print the email content to the console
    soup = BeautifulSoup(html_content, 'html.parser')
    email_body = soup.body.get_text()

    print(f"""
    Email Content:
    To: {org_email}
    From: {from_email}
    {email_body}
    """)

    try:
        sg = SendGridAPIClient("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")
        response = sg.send(email_message)
        print(f"Email sent to {org_email} with meeting link: {meeting_link}. Status code: {response.status_code}")
        if response.status_code == 202:
            return jsonify({'message': 'Email sent successfully'}), 200
        else:
            return jsonify({'message': 'Failed to send email'}), 500
    except Exception as e:
        print(f"Error sending email: {str(e)}")
        return jsonify({'message': 'Error sending email'}), 500

def generate_meeting_link():
    # Generate a random string for the meeting link
    return f"https://example.com/meeting/{secrets.token_urlsafe(10)}"

if __name__ == '__main__':
    app.run(port=3001, debug=True)
