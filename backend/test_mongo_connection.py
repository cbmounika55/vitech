from pymongo import MongoClient, errors
from urllib.parse import quote_plus
import os

# MongoDB credentials
username = quote_plus(os.getenv('MONGO_USERNAME', 'CB2024'))
password = quote_plus(os.getenv('MONGO_PASSWORD', 'Cab123'))

# Connection string
connection_string = f'mongodb+srv://{username}:{password}@virtualai.c4wot6c.mongodb.net/?retryWrites=true&w=majority&appName=VirtualAI'

try:
    # Initialize the MongoDB client
    client = MongoClient(connection_string)
    db = client["your-database"]  # Replace "your-database" with your actual database name
    collection = db["contact_info"]  # Replace "contact_info" with your actual collection name

    # Test the connection
    db.command("ping")
    print("MongoDB connection is active")

    # Now you can perform MongoDB operations like inserting data into the collection
except errors.ConnectionFailure as e:
    print(f"Could not connect to MongoDB: {e}")
except Exception as e:
    print(f"Error initializing MongoDB client: {e}")
