from flask import Flask, request, jsonify
from flask_cors import CORS
from pymongo import MongoClient
from urllib.parse import quote_plus

# MongoDB credentials
username = quote_plus('CB2024')
password = quote_plus('Cab123')

# Connect to MongoDB Atlas
client = MongoClient(f'mongodb+srv://{username}:{password}@virtualai.c4wot6c.mongodb.net/?retryWrites=true&w=majority')
db = client["AgencyProfile"]
collection = db["Basis_info"]

app = Flask(__name__)
CORS(app)  # Enable CORS for all routes

@app.route('/api/basic_info', methods=['POST'])
def save_basic_info():
    data = request.json
    if not data:
        return jsonify({"error": "No data provided"}), 400

    try:
        collection.insert_one(data)
        return jsonify({"message": "Data saved successfully"}), 201
    except Exception as e:
        return jsonify({"error": str(e)}), 500

@app.route('/api/get_basic_info', methods=['GET'])
def get_basic_info():
    try:
        data = list(collection.find({}, {'_id': 0}))
        return jsonify(data), 200
    except Exception as e:
        return jsonify({"error": str(e)}), 500

if __name__ == '__main__':
    app.run(port=5000, debug=True)
